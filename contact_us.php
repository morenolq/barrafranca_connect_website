<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

      function verifyExtension() {
        filename = document.getElementById("fileName").value;
        extension = filename.split('.').pop();
        if (extension === "exe" || extension === "dmgß"){
          alert("Non è possibile inserire file eseguibili.");
          document.getElementById("fileName").value = "";
        }
      }

      function validateContactForm(){

        nome_contact = document.getElementById("nome_contact").value;
        soggetto_contact = document.getElementById("soggetto_contact").value;
        email_contact = document.getElementById("email_contact").value;
        descrizione_contact = document.getElementById("descrizione_contact").value;

        if (nome_contact.trim() === "" || soggetto_contact.trim() === "" ||
            email_contact.trim() === "" || descrizione_contact.trim() === "" ){
              alert("Attenzione, devono essere inseriti tutti i campi.");
              return;
            }

        if (!isValidEmail(email_contact)){
          alert("L'email inserita non è valida.");
          return;
        }

        document.getElementById("contact_us_script").submit();

      }
  </script>
</head>
<body>

  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h4 class="header center orange-text">Contattaci per dirci ciò che pensi o per proporci qualcosa.</h4>
      <center>

        <form class="col s12" method="post" action="PHP_script/contact_us_script.php" id="contact_us_script" enctype="multipart/form-data">
          <div class="row">
            <div class="input-field col s12">
              <input id="nome_contact" name="nome_contact" type="text" class="validate">
              <label for="nome_contact">Il tuo nome</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="soggetto_contact" name="soggetto_contact" type="text" class="validate">
              <label for="soggetto_contact">Di cosa vuoi parlarci</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="email_contact" name="email_contact" type="email" class="validate">
              <label for="email_contact">Email</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">mode_edit</i>
              <textarea id="descrizione_contact" name="descrizione_contact" class="materialize-textarea"></textarea>
              <label for="descrizione_contact">Descrizione</label>
           </div>
          </div>
          <div class='file-field input-field'>
            <div class='btn waves-effect red'>
              <span>Allegato</span>
              <input id='AttachmentFile' name='AttachmentFile' type='file' single>
            </div>
            <div class='file-path-wrapper'>
              <input id='fileName' name='fileName' onchange=\"verifyExtension()\" class='file-path validate' type='text' placeholder='Seleziona Allegato (Non Obbligatorio)'>
            </div>
          </div>

          <button type="button" class="btn waves-effect blue" onclick="validateContactForm()">Invio
            <i class="material-icons right">send</i>
          </button>
        </form>


      </center>

    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'"  class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

</body>
</html>
