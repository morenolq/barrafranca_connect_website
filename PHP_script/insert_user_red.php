<?php

include("./utility_php_bc.php");

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHome("Errore di connessione con il Database");
}

if (!isset($_POST["nome_negozio"]) ||
    !isset($_POST["email_negozio"]) ||
    !isset($_POST["telefono_negozio"]) ||
    !isset($_POST["indirizzo_negozio"]) ||
    !isset($_POST["categoria_negozio"]) ||
    !isset($_POST["descrizione_negozio"]) ||
    !isset($_POST["come_contattarti"]) ||
    !isset($_POST["email_account_red"]) ||
    !isset($_POST["password_account_red1"]) ||
    !isset($_POST["password_account_red2"])
    ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_negozio = mysqli_real_escape_string($conn, $_POST["nome_negozio"]);
$email_negozio = mysqli_real_escape_string($conn, $_POST["email_negozio"]);
$telefono_negozio = mysqli_real_escape_string($conn, $_POST["telefono_negozio"]);
$indirizzo_negozio = mysqli_real_escape_string($conn, $_POST["indirizzo_negozio"]);
$giorno_chiusura_negozio = mysqli_real_escape_string($conn, $_POST["giorno_chiusura_negozio"]);
$orario_negozio = mysqli_real_escape_string($conn, $_POST["orario_negozio"]);
$categoria_negozio = mysqli_real_escape_string($conn, $_POST["categoria_negozio"]);
$descrizione_negozio = mysqli_real_escape_string($conn, $_POST["descrizione_negozio"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);
$nome_utente = mysqli_real_escape_string($conn, $_POST["email_account_red"]);
$password_account_red1 = mysqli_real_escape_string($conn, $_POST["password_account_red1"]);
$password_account_red2 = mysqli_real_escape_string($conn, $_POST["password_account_red2"]);

if (isEmpty($nome_negozio) ||
    isEmpty($email_negozio) ||
    isEmpty($telefono_negozio) ||
    isEmpty($indirizzo_negozio) ||
    isEmpty($categoria_negozio) ||
    isEmpty($descrizione_negozio) ||
    isEmpty($come_contattarti) ||
    isEmpty($nome_utente) ||
    isEmpty($password_account_red1) ||
    isEmpty($password_account_red2)
  ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}

if ($password_account_red1 !== $password_account_red2){
  returnHomeScript("Errore, password diverse, riprovare.");
  exit;
}

if (!isEmail($nome_utente)){
  returnHomeScript("Errore, email non valida.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "SELECT * FROM negozi_barrafranca WHERE nome_negozio = '".$nome_negozio."'";

  if (!($result = mysqli_query($conn, $sql))){
    returnHomeScript("Errore interno, riprova più tardi");
    exit;
  }

  if (mysqli_num_rows($result) > 0){
    throw new Exception("Errore, Negozio gia' registrato");
  }

  $sql = "INSERT INTO negozi_barrafranca (nome_negozio, indirizzo_negozio, email_negozio, telefono_negozio, categoria_negozio, giorno_chiusura_negozio, orario_negozio, descrizione_negozio, come_contattarti, user_red)
  VALUES ('".$nome_negozio."', '".$indirizzo_negozio."', '".$email_negozio."', '".$telefono_negozio."', '".$categoria_negozio."', '".$giorno_chiusura_negozio."', '".$orario_negozio."', '".$descrizione_negozio."', '".$come_contattarti."', true)";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore inserimento, riprova più tardi.");
  }

  $md5_password = md5($password_account_red1);
  $data_pagamento = date('Y-m-d', strtotime("+90 day"));
  $sql = "INSERT INTO login_red (nome_utente, password, data_pagamento, nome_negozio)
  VALUES ('".$nome_utente."', '".$md5_password."', '".$data_pagamento."', '".$nome_negozio."')";

  if (!($result = mysqli_query($conn, $sql))){
    mysqli_rollback($conn);
    throw new Exception("Errore inserimento, probabilmente un account con questa email esiste già, se non è così riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore inserimento, riprova più tardi.");
  } else {
    mysqli_close($conn);
    $body = "Salve, la registrazione a Barrafranca Connect è avvenuta con successo<br><br>
             Le credenziali sono:<br>
             Nome Utente: ".$nome_utente."<br>
             Password: ".$password_account_red1."<br><br>
             Grazie per la registrazione,<br>
             Il Team di Barrafranca Connect";

     $body2 = "Salve, Registrazione nuovo USER RED<br><br>
              Le credenziali sono:<br>
              Nome Utente: ".$nome_utente."<br><br>
              Grazie per la registrazione,<br>
              Il Team di Barrafranca Connect";

    if (sendEmail($nome_utente, "Registrazione Barrafranca Connect", $body)){
      sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO USER RED Barrafranca Connect", $body2);
      returnHomeScript("Registrazione avvenuta con successo. Riceverai un'email di conferma.");
      exit;
    } else {
      returnHomeScript("Registrazione avvenuta con successo.");
      exit;
    }

  }

} catch (Exception $e){
  mysqli_rollback($conn);
  returnHomeScript($e->getMessage());
  exit;
}

?>
