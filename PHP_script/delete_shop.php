<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_negozio'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_shop = mysqli_real_escape_string($conn, $_GET['nome_negozio']);

if (isEmpty($name_transfer_shop)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}
$sql = "SELECT * FROM negozi_barrafranca WHERE nome_negozio='".$name_transfer_shop."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Negozio non trovato.");
  exit;
}

$sql = "DELETE FROM negozi_barrafranca WHERE nome_negozio ='".$name_transfer_shop."';";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Negozio " . $name_transfer_shop . " cancellato correttamente");
  exit;
} else {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Attenzione errore nella cancellazione di " . $name_transfer_shop);
  exit;
}

mysqli_close($conn);

//USAGE http://mlqlab.co.nf/BO_SERVER/SCRIPT_DB/move_shop.php?nome_negozio=QUIILNOME

?>
