<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_offerta'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_offer = mysqli_real_escape_string($conn, $_GET['nome_offerta']);

if (isEmpty($name_transfer_offer)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM nuove_offerte_barrafranca WHERE nome_offerta='".$name_transfer_offer."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Offerta non trovata.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "INSERT INTO offerte_barrafranca (nome_offerta, nome_negozio, data_inizio_offerta, data_fine_offerta, email_contatto_offerta, link_pdf, links_img)
  SELECT * FROM nuove_offerte_barrafranca WHERE nome_offerta ='".$name_transfer_offer."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore inserimento Offerta ".$name_transfer_offer.", riprova più tardi.");
  }

  $sql = "DELETE FROM nuove_offerte_barrafranca WHERE nome_offerta ='".$name_transfer_offer."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore delete Offerta ".$name_transfer_offer.", riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore commit Offerta ".$name_transfer_offer.", riprova più tardi.");
  } else {
    mysqli_close($conn);
    goBackAdminActionScript("Offerta ".$name_transfer_offer." trasferito correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goBackAdminActionScript($e->getMessage());
  exit;
}

?>
