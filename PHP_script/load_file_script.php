<?php

session_start();
include("./utility_php_bc.php");

if(!isset($_SESSION['username']) || !isset($_SESSION['nome_negozio']) || !isset($_SESSION['role']) ){
  returnHomeScript("Errore interno, prova a rifare il login");
  exit;
}

if($_SESSION["role"]!=="USER_RED"){
  returnHomeScript("Errore, Non hai i diritti per questa funzionalità.");
  exit;
}

if (!isset($_FILES['PDFFile']) || !isset($_FILES['PDFFile']['tmp_name']) || !isset($_FILES['PDFFile']['name']) || !is_uploaded_file($_FILES['PDFFile']['tmp_name'])) {
  goToUserRedHomeScript("Errore nella ricezione del file, riprovare.");
  exit;
} else {

  $conn = getConnection();
  if (!$conn) {
      goToUserRedHomeScript("C'è stato un problema nei nostri server, ci scusiamo per l'inconveniente, riprovare più tardi.");
      exit;
  }

  mysqli_query($conn, "SET NAMES 'utf8'");
  mysql_set_charset("utf8");

  $userfile_tmp = mysqli_real_escape_string($conn, $_FILES['PDFFile']['tmp_name']);
  $userfile_name = mysqli_real_escape_string($conn, $_FILES['PDFFile']['name']);
  $ext = pathinfo($userfile_name, PATHINFO_EXTENSION);
  if ($ext !== "pdf"){
    goToUserRedHomeScript("Il file ricevuto non è un pdf, riprovare.");
    exit;
  }
  $file_name = removeAccents("../PDF_OFFERS/".strtr($_SESSION['nome_negozio'], array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);
  if (move_uploaded_file($userfile_tmp, $file_name)) {
    if (!isset($_POST["nome_offerta"]) ||
        !isset($_POST["startDate"]) ||
        !isset($_POST["endDate"])
        ){
          goToUserRedHomeScript("Errore nell'invio del form, riprovare.");
          exit;
    }
    $nome_offerta = mysqli_real_escape_string($conn, $_POST["nome_offerta"]);
    $nome_negozio = mysqli_real_escape_string($conn, $_SESSION['nome_negozio']);
    $data_inizio_offerta = mysqli_real_escape_string($conn, $_POST["startDate"]);
    $data_fine_offerta = mysqli_real_escape_string($conn, $_POST["endDate"]);
    $email_contatto_offerta = mysqli_real_escape_string($conn, $_SESSION['username']);
    $link_pdf = removeAccents("./PDF_OFFERS/".strtr($_SESSION['nome_negozio'], array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);

    if (isEmpty($nome_offerta) ||
        isEmpty($nome_negozio) ||
        isEmpty($data_inizio_offerta) ||
        isEmpty($data_fine_offerta) ||
        isEmpty($email_contatto_offerta) ||
        isEmpty($link_pdf)
      ){
        goToUserRedHomeScript("Errore nell'invio del form, riprovare.");
        exit;
    }

    $sql = "INSERT INTO offerte_barrafranca (nome_offerta, nome_negozio, data_inizio_offerta, data_fine_offerta, email_contatto_offerta, link_pdf)
    VALUES ('".$nome_offerta."', '".$nome_negozio."', '".$data_inizio_offerta."', '".$data_fine_offerta."', '".$email_contatto_offerta."', '".$link_pdf."')
    ON DUPLICATE KEY UPDATE nome_offerta = '".$nome_offerta."' , data_inizio_offerta = '".$data_inizio_offerta."', data_fine_offerta = '".$data_fine_offerta."',
    link_pdf = '".$link_pdf."', email_contatto_offerta = '".$email_contatto_offerta."'";


    if (mysqli_query($conn, $sql)) {
        mysqli_close($conn);
        goToUserRedHomeScript("Il caricamento è avvenuto correttamente! Grazie.");
        exit;
    } else {
        mysqli_close($conn);
        goToUserRedHomeScript("Errore durante il caricamento. Riprova.");
        exit;
    }

  }else{
    goToUserRedHomeScript("Errore nel caricamento del file, riprovare.");
    exit;
  }
}



 ?>
