<?php

include("utility_php_bc.php");
// Create connection
$conn = getConnection();
// Check connection

if (!$conn) {
  returnHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["login-type"]) ||
    !isset($_POST["login-email"])
    ){
  returnHomeScript("Errore nel form, riprovare.");
  exit;
}

$type_login = mysqlCleaner($conn, $_POST["login-type"]);
$username_user = mysqlCleaner($conn, $_POST["login-email"]);

if (isEmpty($type_login) ||
    isEmpty($username_user)
  ){
  returnHomeScript("Errore nel form, riprovare.");
  exit;
}

if ($type_login==="USER_RED"){
  $sql = "SELECT * FROM login_red WHERE nome_utente='".$username_user."'";
} else if($type_login==="ADMINISTRATOR"){
  $sql = "SELECT * FROM login_pc WHERE nome_utente='".$username_user."'";
} else if($type_login==="CREATOR"){
  $sql = "SELECT * FROM login_creators WHERE email_creatore='".$username_user."'";
} else if($type_login==="MUNICIPALITY"){
  $sql = "SELECT * FROM login_comune WHERE nome_utente='".$username_user."'";
} else {
  returnHomeScript("Si è verificato un errore nel recupero password, riprova.");
  exit;
}

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  //ok
} else {
  returnHomeScript("Errore, utente non trovato.");
  exit;
}

$temp_psw = randomString(10);
$temp_psw_md5 = md5($temp_psw);

if ($type_login==="USER_RED"){
  //$sql = "UPDATE login_red SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_red SET password = '".$temp_psw_md5."' WHERE nome_utente='".$username_user."'";
} else if($type_login==="ADMINISTRATOR"){
  //$sql = "UPDATE login_pc SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_pc SET password = '".$temp_psw_md5."' WHERE nome_utente='".$username_user."'";
} else if($type_login==="CREATOR"){
  $sql = "UPDATE login_creators SET password = '".$temp_psw_md5."' WHERE email_creatore='".$username_user."'";
} else if($type_login==="MUNICIPALITY"){
  //$sql = "UPDATE login_pc SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_comune SET password = '".$temp_psw_md5."' WHERE nome_utente='".$username_user."'";
} else {
  returnHomeScript("Si è verificato un errore nel recupero password, riprova.");
  exit;
}

$result = mysqli_query($conn, $sql);

if ($result){
  mysqli_close($conn);
  //send email

  $body = "Buongiorno, è stato richiesto il cambiamento della password di Barrafranca Connect<br><br>
                 Le nuove credenziali sono:<br>
                 Nome Utente: ".$username_user."<br>
                 Password Temporanea: ".$temp_psw."<br>
                 è consigliato di cambiare la password al primo accesso.<br><br>
                 Il Team di Barrafranca Connect";

  if (!sendEmail($username_user, "Reset Password Barrafranca Connect", $body)){
    returnHomeScript("Errore, non è stato possibile inviare la mail, riprova.");
    exit;
  } else {
    returnHomeScript("Una mail con la password temporanea è stata inviata al tuo indirizzo email ".$username_user.". Entro 5 minuti verrà ricevuta, se non trovata, controlla la cartella Spam.");
    exit;
  }


} else {
  mysqli_close($conn);
  returnHomeScript("Errore: non è stato possibile modificare la password per " . $username_user);
  exit;
}

mysqli_close($conn);

 ?>
