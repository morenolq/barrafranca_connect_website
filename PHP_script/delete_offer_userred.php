<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="USER_RED"){
  returnHomeScript("Errore, non hai il diritto di entrare in questa sezione del sito.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goToUserRedHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_offerta'])){
  goToUserRedHomeScript("Errore nell'invio della richiesta");
  exit;
}

if (!isset($_SESSION["nome_negozio"])){
  goToUserRedHomeScript("Errore nell'invio della richiesta, rifare l'accesso");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_offer = mysqli_real_escape_string($conn, $_GET["nome_offerta"]);
$nome_negozio = mysqli_real_escape_string($conn, $_SESSION["nome_negozio"]);


if (isEmpty($name_transfer_offer) || isEmpty($nome_negozio)){
  goToUserRedHomeScript("Errore nell'invio della richiesta");
  exit;
}

$sql = "SELECT *
        FROM offerte_barrafranca
        WHERE nome_offerta='".$name_transfer_offer."' AND nome_negozio='".$nome_negozio."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
    goToUserRedHomeScript("Errore: Evento non trovato.");
    exit;
}


$sql = "DELETE FROM offerte_barrafranca
        WHERE nome_offerta ='".$name_transfer_offer."' AND nome_negozio='".$nome_negozio."';;";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goToUserRedHomeScript("Offerta " . $name_transfer_offer . " eliminato correttamente");
  exit;
} else {
  mysqli_close($conn);
  goToUserRedHomeScript("Errore nella cancellazione di " . $name_transfer_offer);
  exit;
}

mysqli_close($conn);

?>
