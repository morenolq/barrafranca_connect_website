<?php

include("../PHP_script/utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    returnHomeScript("C'è stato un problema nei nostri server, ci scusiamo per l'inconveniente, riprovare più tardi.");
    exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["nome_negozio"]) ||
    !isset($_POST["email_negozio"]) ||
    !isset($_POST["telefono_negozio"]) ||
    !isset($_POST["indirizzo_negozio"]) ||
    !isset($_POST["categoria_negozio"]) ||
    !isset($_POST["descrizione_negozio"]) ||
    !isset($_POST["come_contattarti"])
    ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}

$nome_negozio = mysqli_real_escape_string($conn, $_POST["nome_negozio"]);
$email_negozio = mysqli_real_escape_string($conn, $_POST["email_negozio"]);
$telefono_negozio = mysqli_real_escape_string($conn, $_POST["telefono_negozio"]);
$indirizzo_negozio = mysqli_real_escape_string($conn, $_POST["indirizzo_negozio"]);
$giorno_chiusura_negozio = mysqli_real_escape_string($conn, $_POST["giorno_chiusura_negozio"]);
$orario_negozio = mysqli_real_escape_string($conn, $_POST["orario_negozio"]);
$categoria_negozio = mysqli_real_escape_string($conn, $_POST["categoria_negozio"]);
$descrizione_negozio = mysqli_real_escape_string($conn, $_POST["descrizione_negozio"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);

if (isEmpty($nome_negozio) ||
    isEmpty($email_negozio) ||
    isEmpty($telefono_negozio) ||
    isEmpty($indirizzo_negozio) ||
    isEmpty($categoria_negozio) ||
    isEmpty($descrizione_negozio) ||
    isEmpty($come_contattarti)
  ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}


$sql = "INSERT INTO nuovi_negozi_barrafranca (nome_negozio, indirizzo_negozio, email_negozio, telefono_negozio, categoria_negozio, giorno_chiusura_negozio, orario_negozio, descrizione_negozio, come_contattarti)
VALUES ('".$nome_negozio."', '".$indirizzo_negozio."', '".$email_negozio."', '".$telefono_negozio."', '".$categoria_negozio."', '".$giorno_chiusura_negozio."', '".$orario_negozio."', '".$descrizione_negozio."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è avvenuta la registrazione di un utente BLUE che richiede l'approvazione<br><br>
             Le credenziali sono:<br>
             Nome negozio: ".$nome_negozio."<br><br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "Registrazione BLUE Barrafranca Connect", $body);
    returnHomeScript("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    exit;
} else {
    mysqli_close($conn);
    returnHomeScript("Errore durante il caricamento. Molto probabilmente è già stata fatta una richiesta con questo nome ed è in corso di approvazione.");
    exit;
}


?>
