<?php

function echoPhp($string){
	$string = str_replace('\'', '\\\'', $string);
	$string = str_replace('\"', '\\\"', $string);
	echo $string;
}

function randomString($length = 6) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function mysqlCleaner($conn, $data){
	$data = mysqli_real_escape_string($conn, $data);
	$data = stripslashes($data);
	$data = htmlentities($data);
	return $data;
	//or in one line code
	//return(stripslashes(mysql_real_escape_string($data)));
}

function encryptPassword($password, $sale){

	$saltedPW =  $password . $sale;
	$hashedPW = hash('sha256', $saltedPW);
	return $hashedPW;

}

function destroysession(){
	$_SESSION = array();
	if (ini_get("session.use_cookies")){
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time()- 3600*24, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	}
	session_destroy();
}

function goBackAdminActionScript($stringCookie, $message){
	setcookie($stringCookie, $message, time() + 5, "/");
	if(isset($_SERVER['HTTP_REFERER'])) {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
		exit;
	} else{
		goToAdminHomeScript($message);
	}
}

function returnHomeScript($message){
	setcookie("messageHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../index.php");
}

function returnHome($message){
	setcookie("messageHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./index.php");
}

function goToAdminHomeScript($message){
	setcookie("messageAdminHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../admin_home.php");
}

function goToAdminHome($message){
	setcookie("messageAdminHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./admin_home.php");
}

function goToUserRedHomeScript($message){
	setcookie("messageUserRedHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../userred_home.php");
}

function goToUserRedHome($message){
	setcookie("messageUserRedHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./userred_home.php");
}

function goToCreatorHomeScript($message){
	setcookie("messageCreatorHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../creator_home.php");
}

function goToCreatorHome($message){
	setcookie("messageCreatorHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./creator_home.php");
}

function goToMunicipalityHomeScript($message){
	setcookie("messageMunicipalityHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../municipality_home.php");
}

function goToMunicipalityHome($message){
	setcookie("messageMunicipalityHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./municipality_home.php");
}

function returnErrorLoginScript($message){
	setcookie("messageLoginBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../login.php");
}

function returnErrorLogin($message){
	setcookie("messageLoginBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./login.php");
}

function returnErrorRecoverScript($message){
	setcookie("messageLoginBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../recover_password.php");
}

function returnErrorRecover($message){
	setcookie("messageLoginBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ./recover_password.php");
}

function getConnection(){

	$servername = "db689126687.db.1and1.com";
	$username = "dbo689126687";
	$password = "LogicSwift1993";
	$dbname = "db689126687";

  //$servername = "localhost";
  //$username = "root";
  //$password = "";
  //$dbname = "2307609_barrafrancaonline";

  // Create connection
  $conn = @mysqli_connect($servername, $username, $password, $dbname);
  // Check connection
	return $conn;


}

function getConnectionScript(){

  $servername = "db689126687.db.1and1.com";
  $username = "dbo689126687";
  $password = "LogicSwift1993";
  $dbname = "db689126687";

  //$servername = "localhost";
  //$username = "root";
  //$password = "";
  //$dbname = "2307609_barrafrancaonline";

  // Create connection
  $conn = @mysqli_connect($servername, $username, $password, $dbname);
  // Check connection
  if (!$conn) {
		returnHomeScript("Connessione Temporaneamente non disponibile");
		exit;
  } else {
    return $conn;
  }


}

function printCardRed($message){
	echo "<div class='row'>";
		echo "<div class='col s12 m12'>";
			echo "<div class='card-panel red'>";
				echo "<span class='white-text flow-text'>" . $message;
				echo "</span>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
}

function printCardGreen($message){
	echo "<div class='row'>";
		echo "<div class='col s12 m12'>";
			echo "<div class='card-panel green'>";
				echo "<span class='white-text flow-text'>" . $message;
				echo "</span>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
}

function printCardBlue($message){
	echo "<div class='row'>";
		echo "<div class='col s12 m12'>";
			echo "<div class='card-panel blue'>";
				echo "<span class='white-text flow-text'>" . $message;
				echo "</span>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
}

function printCardOrange($message){
	echo "<div class='row'>";
		echo "<div class='col s12 m12'>";
			echo "<div class='card-panel orange accent-4'>";
				echo "<span class='white-text flow-text'>" . $message;
				echo "</span>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
}

function printCardTeal($message){
	echo "<div class='row'>";
		echo "<div class='col s12 m12'>";
			echo "<div class='card-panel teal'>";
				echo "<span class='white-text flow-text'>" . $message;
				echo "</span>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
}

function printNewEvents(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  nuovi_eventi_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Evento</th>";
				echo "<th>Data Evento</th>";
				echo "<th>Ora Evento</th>";
				echo "<th>Email Evento</th>";
				echo "<th>Telefono Evento</th>";
				echo "<th>Nome Organizzatore Evento</th>";
				echo "<th>Descrizione Evento</th>";
				echo "<th>Come Contattare</th>";
				echo "<th>Valida</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_evento"]."'>";

	      echo "<td>".$row["nome_evento"]."</td>";
				echo "<td>".$row["data_evento"]."</td>";
				echo "<td>".$row["ora_evento"]."</td>";

				if(!empty($row["email_evento"])){
          echo "<td>".$row["email_evento"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_evento"])){
          echo "<td>".$row["telefono_evento"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["nome_organizzatore_evento"]."</td>";
				echo "<td>".$row["descrizione_evento"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect green btn' onclick=\"validateEvent('".addslashes($row["nome_evento"])."')\">Valida</a><td>";
	      echo "<br>";
      }

			echo "</tbody></table></div>";

	  } else {
			printCardBlue("Nessun Nuovo Evento.");
	  }

	}

}

function printEvents(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  eventi_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Evento</th>";
				echo "<th>Data Evento</th>";
				echo "<th>Ora Evento</th>";
				echo "<th>Email Evento</th>";
				echo "<th>Telefono Evento</th>";
				echo "<th>Nome Organizzatore Evento</th>";
				echo "<th>Descrizione Evento</th>";
				echo "<th>Come Contattare</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_evento"]."'>";

	      echo "<td>".$row["nome_evento"]."</td>";
				echo "<td>".$row["data_evento"]."</td>";
				echo "<td>".$row["ora_evento"]."</td>";

				if(!empty($row["email_evento"])){
          echo "<td>".$row["email_evento"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_evento"])){
          echo "<td>".$row["telefono_evento"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["nome_organizzatore_evento"]."</td>";
				echo "<td>".$row["descrizione_evento"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteEvent('".addslashes($row["nome_evento"])."')\">Elimina</a><td>";
	      echo "<br>";
      }

			echo "</tbody></table></div>";

	  } else {
			printCardRed("Nessun Evento.");
	  }

	}

}

function printNewShops(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  nuovi_negozi_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Negozio</th>";
				echo "<th>Indirizzo Negozio</th>";
				echo "<th>Descrizione Negozio</th>";
				echo "<th>Telefono Negozio</th>";
				echo "<th>Email Negozio</th>";
				echo "<th>Orario Negozio</th>";
				echo "<th>Giorno Chiusura Negozio</th>";
				echo "<th>Categoria Negozio</th>";
				echo "<th>Come Contattarti Negozio</th>";
				echo "<th>User Red</th>";
				echo "<th>Valida</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_negozio"]."'>";

	      echo "<td>".$row["nome_negozio"]."</td>";
				echo "<td>".$row["indirizzo_negozio"]."</td>";
				echo "<td>".$row["descrizione_negozio"]."</td>";

				if(!empty($row["telefono_negozio"])){
          echo "<td>".$row["telefono_negozio"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				if(!empty($row["email_negozio"])){
          echo "<td>".$row["email_negozio"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["orario_negozio"])){
          echo "<td>".$row["orario_negozio"]."</td>";
	      } else {
					echo "<td>NO ORARIO</td>";
				}

				if(!empty($row["giorno_chiusura_negozio"])){
          echo "<td>".$row["giorno_chiusura_negozio"]."</td>";
	      } else {
					echo "<td>NO GIORNO CHIUSURA</td>";
				}

				echo "<td>".$row["categoria_negozio"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td>".$row["user_red"]."</td>";
				echo "<td><a class='waves-effect green btn' onclick=\"validateShop('".addslashes($row["nome_negozio"])."')\">Valida</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			echo "<div class='row'>";
				echo "<div class='col s12 m12'>";
					echo "<div class='card-panel blue darken-2'>";
						echo "<span class='white-text'>Nessun nuovo negozio.<br>";
						echo "</span>";
					echo "</div>";
				echo "</div>";
			echo "</div>";
	  }
	}
}

function printShops(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  negozi_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Negozio</th>";
				echo "<th>Indirizzo Negozio</th>";
				echo "<th>Descrizione Negozio</th>";
				echo "<th>Telefono Negozio</th>";
				echo "<th>Email Negozio</th>";
				echo "<th>Orario Negozio</th>";
				echo "<th>Giorno Chiusura Negozio</th>";
				echo "<th>Categoria Negozio</th>";
				echo "<th>Come Contattarti Negozio</th>";
				echo "<th>User Red</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_negozio"]."'>";

	      echo "<td>".$row["nome_negozio"]."</td>";
				echo "<td>".$row["indirizzo_negozio"]."</td>";
				echo "<td>".$row["descrizione_negozio"]."</td>";

				if(!empty($row["telefono_negozio"])){
          echo "<td>".$row["telefono_negozio"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				if(!empty($row["email_negozio"])){
          echo "<td>".$row["email_negozio"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["orario_negozio"])){
					echo "<td>".$row["orario_negozio"]."</td>";
				} else {
					echo "<td>NO ORARIO</td>";
				}

				if(!empty($row["giorno_chiusura_negozio"])){
					echo "<td>".$row["giorno_chiusura_negozio"]."</td>";
				} else {
					echo "<td>NO GIORNO CHIUSURA</td>";
				}

				echo "<td>".$row["categoria_negozio"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td>".$row["user_red"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteShop('".addslashes($row["nome_negozio"])."')\">Elimina</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessun Negozio.");
	  }
	}
}

function printNewJobs(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  nuovi_lavori_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Lavoro</th>";
				echo "<th>Nome Contatto Lavoro</th>";
				echo "<th>Email Lavoro</th>";
				echo "<th>Telefono Lavoro</th>";
				echo "<th>Indirizzo Lavoro</th>";
				echo "<th>Descrizione Lavoro</th>";
				echo "<th>Come Lavoro</th>";
				echo "<th>Valida</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_lavoro"]."'>";

	      echo "<td>".$row["nome_lavoro"]."</td>";
				echo "<td>".$row["nome_contatto_lavoro"]."</td>";

				if(!empty($row["email_lavoro"])){
          echo "<td>".$row["email_lavoro"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_lavoro"])){
          echo "<td>".$row["telefono_lavoro"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["indirizzo_lavoro"]."</td>";
				echo "<td>".$row["descrizione_lavoro"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect green btn' onclick=\"validateJob('".addslashes($row["nome_lavoro"])."')\">Valida</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessun nuovo lavoro.");
	  }
	}
}

function printJobs(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  lavori_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Lavoro</th>";
				echo "<th>Nome Contatto Lavoro</th>";
				echo "<th>Email Lavoro</th>";
				echo "<th>Telefono Lavoro</th>";
				echo "<th>Indirizzo Lavoro</th>";
				echo "<th>Descrizione Lavoro</th>";
				echo "<th>Come Lavoro</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_lavoro"]."'>";

	      echo "<td>".$row["nome_lavoro"]."</td>";
				echo "<td>".$row["nome_contatto_lavoro"]."</td>";

				if(!empty($row["email_lavoro"])){
          echo "<td>".$row["email_lavoro"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_lavoro"])){
          echo "<td>".$row["telefono_lavoro"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["indirizzo_lavoro"]."</td>";
				echo "<td>".$row["descrizione_lavoro"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteJob('".addslashes($row["nome_lavoro"])."')\">Elimina</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessun lavoro.");
	  }
	}
}

function printNewOffers(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  nuove_offerte_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Offerta</th>";
				echo "<th>Nome Negozio</th>";
				echo "<th>Data Inizio Offerta</th>";
				echo "<th>Data Fine Offerta</th>";
				echo "<th>Email Contatto Offerta</th>";
				echo "<th>Link PDF</th>";
				echo "<th>Link(s) Immagini</th>";
				echo "<th>Valida</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_offerta"]."'>";
	      echo "<td>".$row["nome_offerta"]."</td>";
				echo "<td>".$row["nome_negozio"]."</td>";
				echo "<td>".$row["data_inizio_offerta"]."</td>";
				echo "<td>".$row["data_fine_offerta"]."</td>";

				if(!empty($row["email_contatto_offerta"])){
          echo "<td>".$row["email_contatto_offerta"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["link_pdf"])){
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>".$row["link_pdf"]."</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
	      } else {
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>NO LINK PDF</textarea>";
					echo "<label for='textarea1'>Textarea</label></td>";
				}

				if(!empty($row["links_img"])){
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>".$row["links_img"]."</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
	      } else {
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>NO LINKs IMMAGINI</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
				}

				echo "<td><a class='waves-effect green btn' onclick=\"validateOffer('".addslashes($row["nome_offerta"])."')\">Valida</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessuna nuova Offerta");
	  }
	}
}

function printOffers(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  offerte_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Offerta</th>";
				echo "<th>Nome Negozio</th>";
				echo "<th>Data Inizio Offerta</th>";
				echo "<th>Data Fine Offerta</th>";
				echo "<th>Email Contatto Offerta</th>";
				echo "<th>Link PDF</th>";
				echo "<th>Link(s) Immagini</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_offerta"]."'>";
	      echo "<td>".$row["nome_offerta"]."</td>";
				echo "<td>".$row["nome_negozio"]."</td>";
				echo "<td>".$row["data_inizio_offerta"]."</td>";
				echo "<td>".$row["data_fine_offerta"]."</td>";

				if(!empty($row["email_contatto_offerta"])){
          echo "<td>".$row["email_contatto_offerta"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["link_pdf"])){
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>".$row["link_pdf"]."</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
	      } else {
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>NO LINK PDF</textarea>";
					echo "<label for='textarea1'>Textarea</label></td>";
				}

				if(!empty($row["links_img"])){
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>".$row["links_img"]."</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
	      } else {
					echo "<td><textarea id='textarea1' class='materialize-textarea disabled'>NO LINKs IMMAGINI</textarea>";
          echo "<label for='textarea1'>Textarea</label></td>";
				}

				echo "<td><a class='waves-effect red btn' onclick=\"deleteOffer('".addslashes($row["nome_offerta"])."')\">Elimina</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessuna Offerta");
	  }
	}
}

function printNewComproVendo(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  nuovi_comprovendo_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Compro Vendo</th>";
				echo "<th>Prezzo Compro Vendo</th>";
				echo "<th>Nome Contatto Compro Vendo</th>";
				echo "<th>Email Compro Vendo</th>";
				echo "<th>Telefono Compro Vendo</th>";
				echo "<th>Descrizione Compro Vendo</th>";
				echo "<th>Come contattarti</th>";
				echo "<th>Valida</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_comprovendo"]."'>";
	      echo "<td>".$row["nome_comprovendo"]."</td>";
				echo "<td>".$row["prezzo_comprovendo"]."</td>";
				echo "<td>".$row["nome_contatto_comprovendo"]."</td>";

				if(!empty($row["email_comprovendo"])){
          echo "<td>".$row["email_comprovendo"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_comprovendo"])){
          echo "<td>".$row["telefono_comprovendo"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["descrizione_comprovendo"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect green btn' onclick=\"validateComproVendo('".addslashes($row["nome_comprovendo"])."')\">Valida</a><td>";
	      echo "<br>";

      }
			echo "</tbody></table></div>";
	  } else {
			printCardRed("Nessun nuovo Compro Vendo.");
	  }
	}
}

function printComproVendo(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  comprovendo_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Compro Vendo</th>";
				echo "<th>Prezzo Compro Vendo</th>";
				echo "<th>Nome Contatto Compro Vendo</th>";
				echo "<th>Email Compro Vendo</th>";
				echo "<th>Telefono Compro Vendo</th>";
				echo "<th>Descrizione Compro Vendo</th>";
				echo "<th>Come Contattare</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_comprovendo"]."'>";
	      echo "<td>".$row["nome_comprovendo"]."</td>";
				echo "<td>".$row["prezzo_comprovendo"]."</td>";
				echo "<td>".$row["nome_contatto_comprovendo"]."</td>";

				if(!empty($row["email_comprovendo"])){
          echo "<td>".$row["email_comprovendo"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_comprovendo"])){
          echo "<td>".$row["telefono_comprovendo"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["descrizione_comprovendo"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteComproVendo('".addslashes($row["nome_comprovendo"])."')\">Elimina</a><td>";
	      echo "<br>";

      }
			echo "</tbody></table></div>";
	  } else {

			printCardRed("Nessun Compro Vendo.");
	  }
	}
}

function printAdvice(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  avvisi_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Consiglio</th>";
				echo "<th>Descrizione Consiglio</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_consiglio"]."'>";
				echo "<td>".$row["nome_consiglio"]."</td>";
	      echo "<td>".$row["descrizione_consiglio"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteAdvice('".addslashes($row["nome_consiglio"])."')\">Elimina</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {

			printCardRed("Nessun consiglio.");
	  }
	}
}

function printDeleteRequest(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  cancellazione_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Cancellazione</th>";
				echo "<th>Motivo Cancellazione</th>";
				echo "<th>Categoria Cancellazione</th>";
				echo "<th>Come Contattarti Cancellazione</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_cancellazione"]."'>";
				echo "<td>".$row["nome_cancellazione"]."</td>";
	      echo "<td>".$row["motivo_cancellazione"]."</td>";
				echo "<td>".$row["categoria_cancellazione"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteDelete('".addslashes($row["nome_cancellazione"])."')\">Elimina</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {

			printCardRed("Nessuna Cancellazione richiesta.");
	  }
	}
}

function printPharma(){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToAdminHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  farmacie_barrafranca";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToAdminHome("ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn));
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Farmacia</th>";
				echo "<th>Di Turno</th>";
				echo "<th>Cambia</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_farmacia"]."'>";
	      echo "<td>".$row["nome_farmacia"]."</td>";
				echo "<td>".$row["diturno_farmacia"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"setPharma('".addslashes($row["nome_farmacia"])."')\">Metti di Turno</a><td>";
	      echo "<br>";
      }
			echo "</tbody></table></div>";
	  } else {

			printCardRed("Nessun consiglio.");
	  }
	}
}

function verifyUserExist($email){
	$conn = getConnection();
	if (!$conn) {
		returnErrorLogin("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");
	$requestEmail = mysqli_real_escape_string($conn, $email);

	$sql = "SELECT * FROM login_pc WHERE nome_utente='$requestEmail';";
	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) <= 0) {
		returnErrorRecover("Errore: Email non trovata.");
		exit;
	}
}

function validateResetPassword($email, $question, $answer){
	$conn = getConnection();
	if (!$conn) {
		returnErrorLogin("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");

	$requestEmail = mysqli_real_escape_string($conn, $email);
	$requestQuestion = mysqli_real_escape_string($conn, $question);
	$requestAnswer = mysqli_real_escape_string($conn, $answer);

	$sql = "SELECT * FROM login_pc WHERE nome_utente='$requestEmail' AND domanda_segreta='$requestQuestion' AND risposta_segreta='$requestAnswer';";
	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) <= 0) {
		returnErrorRecover("Errore: risposta non corretta.");
		exit;
	}
}

function ownerOffer($username, $nome_negozio){
	$conn = getConnection();
	if (!$conn) {
		returnHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");
	$username = mysqli_real_escape_string($conn, $username);
	$nome_negozio = mysqli_real_escape_string($conn, $nome_negozio);
	$sql = "SELECT * FROM offerte_barrafranca WHERE nome_negozio='$nome_negozio'";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {
		return true;
	} else {
		return false;
	}

}

function printUploadFile(){
	echo "<form action='#'>
					<div class='file-field input-field'>
						<div class='btn'>
							<span>File</span>
							<input type='file'>
						</div>
						<div class='file-path-wrapper'>
							<input class='file-path validate' type='text'>
						</div>
					</div>
				</form>";
}

function printPDF($username, $nome_negozio){
	$conn = getConnection();
	if (!$conn) {
		returnErrorLogin("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");
	$username = mysqli_real_escape_string($conn, $username);
	$nome_negozio = mysqli_real_escape_string($conn, $nome_negozio);
	$sql = "SELECT *
					FROM offerte_barrafranca
					WHERE nome_negozio='$nome_negozio'";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		$link_pdf = $row["link_pdf"];
		echo "<a id='buttonviewPDF' name='".$link_pdf."' class='waves-effect blue btn'>Visualizza PDF Offerta</a><br><br>";
		echo "<a id='buttonviewPDF' name='delete_offer' onclick=\"deleteOffer('".$row["nome_offerta"]."')\" class='waves-effect red btn'>Elimina Offerta Corrente</a><br>";
	} else {
		return false;
	}


}

function ownerEvent($username, $nome_negozio){
	$conn = getConnection();
	if (!$conn) {
		returnErrorLogin("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");
	$username = mysqli_real_escape_string($conn, $username);
	$nome_negozio = mysqli_real_escape_string($conn, $nome_negozio);
	$sql = "SELECT * FROM eventi_barrafranca WHERE email_inserzionista='$username'";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {
		return true;
	} else {
		return false;
	}

}

function ownerNotice($username){
	$conn = getConnection();
	if (!$conn) {
		returnErrorLogin("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	mysqli_query($conn, "SET NAMES 'utf8'");
	$username = mysqli_real_escape_string($conn, $username);
	$sql = "SELECT * FROM avvisi_amministrazione_barrafranca
					WHERE email='$username'
					ORDER BY STR_TO_DATE(data_avviso, '%d/%m/%Y') DESC";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {
		return true;
	} else {
		return false;
	}

}

function printEventsUserRed($username, $nome_negozio){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  returnHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	$username = mysqli_real_escape_string($conn, $username);
	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM  eventi_barrafranca WHERE email_inserzionista='$username'";
	$result = mysqli_query($conn, $sql);


	if (!$result) {
	    echo "<h2>Errore nel recupero degli eventi</h2>";
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Evento</th>";
				echo "<th>Data Evento</th>";
				echo "<th>Ora Evento</th>";
				echo "<th>Email Evento</th>";
				echo "<th>Telefono Evento</th>";
				echo "<th>Nome Organizzatore Evento</th>";
				echo "<th>Descrizione Evento</th>";
				echo "<th>Come Contattare</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_evento"]."'>";

	      echo "<td>".$row["nome_evento"]."</td>";
				echo "<td>".$row["data_evento"]."</td>";
				echo "<td>".$row["ora_evento"]."</td>";

				if(!empty($row["email_evento"])){
          echo "<td>".$row["email_evento"]."</td>";
	      } else {
					echo "<td>NO EMAIL</td>";
				}

				if(!empty($row["telefono_evento"])){
          echo "<td>".$row["telefono_evento"]."</td>";
	      } else {
					echo "<td>NO TELEFONO</td>";
				}

				echo "<td>".$row["nome_organizzatore_evento"]."</td>";
				echo "<td>".$row["descrizione_evento"]."</td>";
				echo "<td>".$row["come_contattarti"]."</td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteEvent('".addslashes($row["nome_evento"])."')\">Elimina</a><td>";
	      echo "<br>";
      }

			echo "</tbody></table></div>";

	  } else {
			printCardRed("Nessun Evento.");
	  }

	}

}

function printNoticesMunicipality($username, $nome_negozio){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  returnHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	$username = mysqli_real_escape_string($conn, $username);
	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM avvisi_amministrazione_barrafranca WHERE email='$username'";
	$result = mysqli_query($conn, $sql);


	if (!$result) {
	    echo "<h2>Errore nel recupero degli avvisi</h2>";
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Avviso</th>";
				echo "<th>Data Avviso</th>";
				echo "<th>Descrizione Avviso</th>";
				echo "<th>Link Avviso</th>";
				echo "<th>PDF Avviso</th>";
				echo "<th>Elimina</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_avviso"]."'>";

	      echo "<td>".$row["nome_avviso"]."</td>";
				echo "<td>".$row["data_avviso"]."</td>";
				echo "<td>".$row["descrizione_avviso"]."</td>";

				if(!empty($row["link_avviso"])){
          echo "<td>".$row["link_avviso"]."</td>";
	      } else {
					echo "<td>NO LINK</td>";
				}

				if(!empty($row["link_pdf_avviso"])){
          echo "<td><a href='http://www.barrafrancaconnect.com/".$row["link_pdf_avviso"]."'>Apri PDF</a></td>";
	      } else {
					echo "<td>NO PDF</td>";
				}

				echo "<td><a class='waves-effect red btn' onclick=\"deleteNotice('".addslashes($row["nome_avviso"])."')\">Elimina</a><td>";
	      echo "<br>";
      }

			echo "</tbody></table></div>";

	  } else {
			printCardRed("Nessun Avviso.");
	  }

	}

}

function printAlbumsCreator($nome_creatore){

	//[Connection + Check]
	$conn = getConnection();
	if (!$conn) {
	  goToCreatorHome("Si è verificato un errore nella parte server, riprova più tardi.");
		exit;
	}

	$nome_creatore = mysqli_real_escape_string($conn, $nome_creatore);
	//[Query]
	mysqli_query($conn, "SET NAMES 'utf8'");
	$sql = "SELECT * FROM album_barrafranca WHERE nome_creatore='$nome_creatore'";
	$result = mysqli_query($conn, $sql);

	if (!$result) {
	    goToCreatorHome("Errore nel recupero degli album.");
			exit;
	} else {
		if (mysqli_num_rows($result) > 0) {
			echo "<div style='overflow-x:auto;'><table class='highlight bordered'><thead><tr><tbody>";
				echo "<th>Nome Album</th>";
				echo "<th>Nome Creatore</th>";
				echo "<th>Descrizione Album</th>";
				echo "<th>Link Personale</th>";
				echo "<th>Cambia Informazioni</th>";
				echo "<th>Cancella Album</th>";
			echo "</tr></thead>";

      while($row = mysqli_fetch_assoc($result)) {
				echo "<tr id='".$row["nome_album"]."'>";
	      echo "<td>".$row["nome_album"]."</td>";
				echo "<td>".$row["nome_creatore"]."</td>";
				echo "<td>".$row["descrizione_album"]."</td>";
				echo "<td>".$row["link_creatore"]."</td>";
				echo "<td><a class='waves-effect green btn' onclick=\"updateAbumData('".addslashes($row["nome_album"])."')\">Cambia_Informazioni</a></td>";
				echo "<td><a class='waves-effect red btn' onclick=\"deleteAlbum('".addslashes($row["nome_album"])."')\">Cancella_Album</a></td>";
				echo "<br>";
      }

			echo "</tbody></table></div>";

	  } else {
			printCardRed("Nessun Album Pubblicato.");
	  }

	}

}

function sendEmail($to, $subject, $body){
	//require "../PHPmailer/PHPMailerAutoload.php";
	require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');

  $mail = new PHPMailer(); // create a new object
  $mail->IsSMTP(); // enable SMTP
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
  $mail->SMTPDebug = 0;
  $mail->SMTPAuth = true; // authentication enabled
  $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
  $mail->Host = "smtp.1and1.com";
  $mail->Port = 25; // 25 or 587 --- 465
  $mail->IsHTML(true);
  $mail->Username = "amministrazione@barrafrancaconnect.com";
  $mail->Password = "LogicSwift1993";
  $mail->SetFrom("amministrazione@barrafrancaconnect.com");
  $mail->addReplyTo( 'amministrazione@barrafrancaconnect.com', 'Amministrazione Barrafranca Connect' );
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AddAddress($to);

	return $mail->send();

}

function sendEmailWithFrom ($from, $to, $subject, $body){
	//require "../PHPmailer/PHPMailerAutoload.php";
	require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');

  $mail = new PHPMailer(); // create a new object
  $mail->IsSMTP(); // enable SMTP
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
  $mail->SMTPDebug = 0;
  $mail->SMTPAuth = true; // authentication enabled
  $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
  $mail->Host = "smtp.1and1.com";
  $mail->Port = 25; // 25 or 587 --- 465
  $mail->IsHTML(true);
  $mail->Username = "amministrazione@barrafrancaconnect.com";
  $mail->Password = "LogicSwift1993";
  $mail->SetFrom($from);
  $mail->addReplyTo($from);
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AddAddress($to);

	return $mail->send();

}

function sendEmailFromAttachment ($from, $to, $subject, $body, $attTempName, $attName){
	//require "../PHPmailer/PHPMailerAutoload.php";
	require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');

  $mail = new PHPMailer(); // create a new object
  $mail->IsSMTP(); // enable SMTP
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
  $mail->SMTPDebug = 0;
  $mail->SMTPAuth = true; // authentication enabled
  $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
  $mail->Host = "smtp.1and1.com";
  $mail->Port = 25; // 25 or 587 --- 465
  $mail->IsHTML(true);
  $mail->Username = "amministrazione@barrafrancaconnect.com";
  $mail->Password = "LogicSwift1993";
  $mail->SetFrom($from);
  $mail->addReplyTo($from);
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AddAddress($to);
	$mail->AddAttachment($attTempName, $attName);

	return $mail->send();

}

function isEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function isEmpty($string) {
	if (empty($string)) {
  	return true;
	} else{
		return false;
	}
}

function Delete($path)
{
  if (is_dir($path) === true){
    $files = array_diff(scandir($path), array('.', '..'));

    foreach ($files as $file){
      Delete(realpath($path) . '/' . $file);
    }

    return rmdir($path);
  }

  else if (is_file($path) === true){
    return unlink($path);
  }

  return false;
}

function removeAccents($str) {
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή', '&');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η', 'e');
  return str_replace($a, $b, $str);
}

function sendNotificationInternal($title, $body, $pageToOpen) {


		$msg = array
		(
			'body'  => $title,
			'title'     => $body,
			'vibrate'   => 1,
			'sound'     => 1,
		);

		$fields = array
		(
			'to'  => "/topics/notices_municipality",
			'notification' => $msg,
			"data" => [
            "pageToOpenInternal" => $pageToOpen,
      ],
		);

		$headers = array
		(
			'Authorization: key=' . "AAAANlg7mz4:APA91bEX6GLfhcO9fM2ztP3rdpS3nVbMfQQtxNIYX57OxeP-XbGEiqk9OFQ1Urq1KYQxmdi6bBONWRv3gDngmHhefwJPF471MagSsk8MJ0b-32ssfxHn498j1VZl6-zJRw3IjnZMHhCJ",
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;
}

?>
