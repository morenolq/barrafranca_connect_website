<?php

include("utility_php_bc.php");
// Create connection
$conn = getConnection();
// Check connection

if (!$conn) {
  returnHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["login-type"]) ||
    !isset($_POST["login-email"]) ||
    !isset($_POST["login-password"])
    ){
  returnHomeScript("Errore nel login, riprovare.");
  exit;
}

$type_login = mysqlCleaner($conn, $_POST["login-type"]);
$username_user = mysqlCleaner($conn, $_POST["login-email"]);
$password_user = mysqlCleaner($conn, $_POST["login-password"]);
$password_user = md5($password_user);

if (isEmpty($type_login) ||
    isEmpty($username_user) ||
    isEmpty($password_user)
  ){
  returnHomeScript("Errore nel login, riprovare.");
  exit;
}

if($type_login==="ADMINISTRATOR"){
  $sql = "SELECT * FROM login_pc WHERE nome_utente='".$username_user."' AND password='".$password_user."'";
} else if($type_login==="USER_RED"){
  $sql = "SELECT * FROM login_red WHERE nome_utente='".$username_user."' AND password='".$password_user."'";
} else if($type_login==="CREATORS"){
  $sql = "SELECT * FROM login_creators WHERE email_creatore='".$username_user."' AND password_creatore='".$password_user."'";
} else if($type_login==="MUNICIPALITY"){
  $sql = "SELECT * FROM login_comune WHERE nome_utente='".$username_user."' AND password='".$password_user."'";
}

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  //instead start session
  session_start();
  if($type_login==="ADMINISTRATOR"){
    $_SESSION["username"] = $username_user;
    $_SESSION["role"] = "ADMINISTRATOR";
    mysqli_close($conn);
    goToAdminHomeScript("Accesso effettutato con successo per " . $username_user ); //go to admin panel
    exit;
  } else if ($type_login==="USER_RED"){
    $row = mysqli_fetch_assoc($result);
    $_SESSION["username"] = $username_user;
    $_SESSION["role"] = "USER_RED";
    $_SESSION["nome_negozio"] = $row["nome_negozio"];
    mysqli_close($conn);
    goToUserRedHomeScript("Accesso effettutato con successo per " . $username_user ); //go to admin panel
    exit;
  } else if ($type_login==="CREATORS"){
    $row = mysqli_fetch_assoc($result);
    $_SESSION["username"] = $username_user;
    $_SESSION["role"] = "CREATOR";
    $_SESSION["nome_creatore"] = $row["nome_creatore"];
    mysqli_close($conn);
    goToCreatorHomeScript("Accesso effettutato con successo per " . $username_user ); //go to creators panel
    exit;
  } else if ($type_login==="MUNICIPALITY"){
    $row = mysqli_fetch_assoc($result);
    $_SESSION["username"] = $username_user;
    $_SESSION["role"] = "MUNICIPALITY";
    mysqli_close($conn);
    goToMunicipalityHomeScript("Accesso effettutato con successo per " . $username_user ); //go to creators panel
    exit;
  } else {
    returnHomeScript("Errore nel login, riprovare.");
  }

} else {
  returnErrorLoginScript("Utente non trovato, registrati o verifica di aver inserito le informazioni corrette.");
}

mysqli_close($conn);



?>
