<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_negozio'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_shop = mysqli_real_escape_string($conn, $_GET['nome_negozio']);

if (isEmpty($name_transfer_shop)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM nuovi_negozi_barrafranca WHERE nome_negozio='".$name_transfer_shop."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Negozio non trovato.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "INSERT INTO negozi_barrafranca (nome_negozio, indirizzo_negozio, descrizione_negozio, telefono_negozio, email_negozio, giorno_chiusura_negozio, orario_negozio, categoria_negozio, come_contattarti, user_red)
  SELECT * FROM nuovi_negozi_barrafranca WHERE nome_negozio ='".$name_transfer_shop."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore inserimento Negozio ".$name_transfer_shop.", riprova più tardi.");
  }

$sql = "DELETE FROM nuovi_negozi_barrafranca WHERE nome_negozio ='".$name_transfer_shop."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore delete Negozio ".$name_transfer_shop.", riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore commit Negozio ".$name_transfer_shop.", riprova più tardi.");
  } else {
    mysqli_close($conn);
    goBackAdminActionScript("Negozio ".$name_transfer_shop." trasferito correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goBackAdminActionScript($e->getMessage());
  exit;
}

?>
