<?php

include("./utility_php_bc.php");
session_start();

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHomeScript("Errore di connessione con il Database");
}

if (!isset($_POST["email_negozio"]) ||
    !isset($_POST["telefono_negozio"]) ||
    !isset($_POST["indirizzo_negozio"]) ||
    !isset($_POST["categoria_negozio"]) ||
    !isset($_POST["descrizione_negozio"]) ||
    !isset($_POST["come_contattarti"])
    ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_negozio = mysqli_real_escape_string($conn, $_SESSION["nome_negozio"]);
$email_negozio = mysqli_real_escape_string($conn, $_POST["email_negozio"]);
$telefono_negozio = mysqli_real_escape_string($conn, $_POST["telefono_negozio"]);
$indirizzo_negozio = mysqli_real_escape_string($conn, $_POST["indirizzo_negozio"]);
$giorno_chiusura_negozio = mysqli_real_escape_string($conn, $_POST["giorno_chiusura_negozio"]);
$orario_negozio = mysqli_real_escape_string($conn, $_POST["orario_negozio"]);
$categoria_negozio = mysqli_real_escape_string($conn, $_POST["categoria_negozio"]);
$descrizione_negozio = mysqli_real_escape_string($conn, $_POST["descrizione_negozio"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);

if (isEmpty($nome_negozio) ||
    isEmpty($email_negozio) ||
    isEmpty($telefono_negozio) ||
    isEmpty($indirizzo_negozio) ||
    isEmpty($categoria_negozio) ||
    isEmpty($descrizione_negozio) ||
    isEmpty($come_contattarti)
  ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riempi tutti i campi e riprova.");
  exit;
}

try {
  mysqli_autocommit($conn, false);
  $sql = "UPDATE negozi_barrafranca
          SET indirizzo_negozio = '".$indirizzo_negozio."' ,
              email_negozio = '".$email_negozio."',
              telefono_negozio = '".$telefono_negozio."',
              categoria_negozio = '".$categoria_negozio."',
              giorno_chiusura_negozio = '".$giorno_chiusura_negozio."',
              orario_negozio = '".$orario_negozio."',
              descrizione_negozio = '".$descrizione_negozio."',
              come_contattarti = '".$come_contattarti."',
              user_red = true
          WHERE nome_negozio = '".$_SESSION["nome_negozio"]."';";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore durante l'aggiornamento delle informazioni, riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore durante l'aggiornamento delle informazioni, riprova più tardi.");
  } else {
    mysqli_close($conn);
    returnHomeScript("Informazioni cambiate con successo.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  returnHomeScript($e->getMessage());
  exit;
}

?>
