<?php

session_start();
include("./utility_php_bc.php");

if (!isset($_SESSION["role"])){
  returnHomeScript("Errore, rifare il login.");
  exit;
}

if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_evento'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_event = mysqli_real_escape_string($conn, $_GET["nome_evento"]);

if (isEmpty($name_transfer_event)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM nuovi_eventi_barrafranca WHERE nome_evento='".$name_transfer_event."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Evento non trovato.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "INSERT INTO eventi_barrafranca (nome_evento, data_evento, ora_evento, email_evento, telefono_evento, nome_organizzatore_evento, descrizione_evento, come_contattarti, email_inserzionista)
          SELECT * FROM nuovi_eventi_barrafranca WHERE nome_evento ='".$name_transfer_event."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore inserimento evento ".$name_transfer_event.", riprova più tardi.");
  }

  $sql = "DELETE FROM nuovi_eventi_barrafranca WHERE nome_evento ='".$name_transfer_event."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore delete evento ".$name_transfer_event.", riprova più tardi.");
  }

  mysqli_close($conn);

  if (!mysqli_commit($conn)){
    throw new Exception("Errore commit evento ".$name_transfer_event.", riprova più tardi.");
  } else {
    mysqli_close($conn);
    goBackAdminActionScript("Evento ".$name_transfer_event." trasferito correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goBackAdminActionScript($e->getMessage());
  exit;
}

?>
