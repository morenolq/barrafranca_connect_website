<?php

include("./utility_php_bc.php");

$nome_contact = $_POST['nome_contact'];
$soggetto_contact = $_POST['soggetto_contact'];
$email_contact = $_POST['email_contact'];
$descrizione_contact = $_POST['descrizione_contact'];

if (isEmpty($nome_contact) ||
    isEmpty($soggetto_contact) ||
    isEmpty($email_contact) ||
    isEmpty($descrizione_contact)){
  returnHomeScript("Errore, dati mancanti, riprova.");
  exit;
}

if (isset($_FILES['AttachmentFile']) &&
    isset($_FILES['AttachmentFile']['tmp_name']) &&
    isset($_FILES['AttachmentFile']['name']) &&
    is_uploaded_file($_FILES['AttachmentFile']['tmp_name'])){
      //With Attachment
      if (sendEmailFromAttachment($email_contact, //from
                    "mlqdevelop@gmail.com", //to
                    $nome_contact. " HA INVIATO ".$soggetto_contact." nel sito di Barrafranca Connect" , // subject
                    $descrizione_contact, // descrizione
                    $_FILES['AttachmentFile']['tmp_name'], //path
                    $_FILES['AttachmentFile']['name'])){ //filename
          returnHomeScript("Grazie, la tua richiesta è stata registrata.");
      } else {
        returnHomeScript("Scusaci, c'è stato un errore nella registrazione della richesta, riprova più tardi.");
        exit;
      }

    } else {
      //no attachment
      if (sendEmailWithFrom($email_contact, //from
                    "mlqdevelop@gmail.com", //to
                    $nome_contact. " HA INVIATO ".$soggetto_contact." nel sito di Barrafranca Connect" , // subject
                    $descrizione_contact)){ // descrizione
          returnHomeScript("Grazie, la tua richiesta è stata registrata.");
      } else {
        returnHomeScript("Scusaci, c'è stato un errore nella registrazione della richesta, riprova più tardi.");
        exit;
      }
    }




 ?>
