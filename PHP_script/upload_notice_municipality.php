<?php

session_start();
include("./utility_php_bc.php");

if (!isset($_SESSION["role"])){
  returnHomeScript("Errore, rifare il login.");
  exit;
}

if ($_SESSION["role"]!=="MUNICIPALITY"){
  returnHomeScript("Errore, permesso non consentito.");
  exit;
}

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHomeScript("Errore di connessione con il Database");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["nome_avviso"]) ||
    !isset($_POST["data_avviso"]) ||
    !isset($_POST["descrizione_avviso"]) ||
    !isset($_SESSION["username"])
    ){
  returnHomeScript("Errore nell'invio del form, riprovare.");
  exit;
}

$nome_avviso = mysqli_real_escape_string($conn, $_POST["nome_avviso"]);
$nome_avviso_notify = $_POST["nome_avviso"];
$data_avviso = mysqli_real_escape_string($conn, $_POST["data_avviso"]);
$descrizione_avviso = mysqli_real_escape_string($conn, $_POST["descrizione_avviso"]);
$email = mysqli_real_escape_string($conn, $_SESSION["username"]);

if (isEmpty($nome_avviso) ||
    isEmpty($data_avviso) ||
    isEmpty($descrizione_avviso) ||
    isEmpty($email)
  ){
  goToMunicipalityHomeScript("Errore nell'invio del form, riprovare.");
  exit;
}

if ($_POST["notify_avviso"]=="on"){
  $notify=true;
}

if (!isset($_FILES['PDFFile']) || !isset($_FILES['PDFFile']['tmp_name']) || !isset($_FILES['PDFFile']['name']) || !is_uploaded_file($_FILES['PDFFile']['tmp_name'])) {
  //NO FILE NOTICE
  if(isset($_POST["link_avviso"])){
    $link_avviso = mysqli_real_escape_string($conn, $_POST["link_avviso"]);
  } else {
    $link_avviso = "";
  }

  $sql = "INSERT INTO avvisi_amministrazione_barrafranca (email, nome_avviso, data_avviso, descrizione_avviso, link_avviso, link_pdf_avviso)
  VALUES ('".$email."', '".$nome_avviso."', '".$data_avviso."', '".$descrizione_avviso."', '".$link_avviso."', '')";
  if (mysqli_query($conn, $sql)) {

      if ($notify){
        //NOTIFY USERS
        $id_notification = sendNotificationInternal("Nuovo avviso dall'amministrazione", $nome_avviso_notify, "avvisi_amministrazione.html");
        $created_date = date("d-m-Y H:i:s");
        $sql = "INSERT INTO notifications_log (id, timestamp_notification)
        VALUES ('".$id_notification."', '".$created_date."')";
        mysqli_query($conn, $sql);
      }
      mysqli_close($conn);
      goToMunicipalityHomeScript("Avviso inserito correttamente! grazie.");
      exit;
  } else {
      mysqli_close($conn);
      goToMunicipalityHomeScript("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
      exit;
  }
} else {

  $userfile_tmp = mysqli_real_escape_string($conn, $_FILES['PDFFile']['tmp_name']);
  $userfile_name = mysqli_real_escape_string($conn, $_FILES['PDFFile']['name']);
  $ext = pathinfo($userfile_name, PATHINFO_EXTENSION);
  if ($ext !== "pdf"){
    goToMunicipalityHomeScript("Il file ricevuto non è un pdf, riprovare.");
    exit;
  }
  $file_name = removeAccents("../PDF_NOTICES/PDF_AVVISO_".strtr($nome_avviso, array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);
  if (move_uploaded_file($userfile_tmp, $file_name)) {
    $link_pdf = removeAccents("./PDF_NOTICES/PDF_AVVISO_".strtr($nome_avviso, array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);

    if(isset($_POST["link_avviso"])){
      $link_avviso = mysqli_real_escape_string($conn, $_POST["link_avviso"]);
    } else {
      $link_avviso = "";
    }

    $sql = "INSERT INTO avvisi_amministrazione_barrafranca (email, nome_avviso, data_avviso, descrizione_avviso, link_avviso, link_pdf_avviso)
    VALUES ('".$email."', '".$nome_avviso."', '".$data_avviso."', '".$descrizione_avviso."', '".$link_avviso."', '".$link_pdf."')";
    if (mysqli_query($conn, $sql)) {

        if ($notify){
          //NOTIFY USERS
          $id_notification = sendNotificationInternal("Nuovo avviso dall'amministrazione", $nome_avviso_notify, "avvisi_amministrazione.html");
          $created_date = date("d-m-Y H:i:s");
          $sql = "INSERT INTO notifications_log (id, timestamp_notification)
          VALUES ('".$id_notification."', '".$created_date."')";
          mysqli_query($conn, $sql);
        }
        mysqli_close($conn);
        goToMunicipalityHomeScript("Avviso inserito correttamente! grazie.");
        exit;
    } else {
        mysqli_close($conn);
        goToMunicipalityHomeScript("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
        exit;
    }

    mysqli_close($conn);
  } else {
    goToMunicipalityHomeScript("Errore nel caricamento del file, riprovare.");
    exit;
  }

}




?>
