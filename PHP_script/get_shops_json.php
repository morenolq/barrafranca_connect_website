<?php

header("Content-Type: text/html;charset=utf-8");

include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "NESSUNRISULTATOBARRAFRANCACONNECTPHPSCRIPT ERRORE CONNECT";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT negozi_barrafranca.nome_negozio,
               negozi_barrafranca.indirizzo_negozio,
               negozi_barrafranca.descrizione_negozio,
               negozi_barrafranca.telefono_negozio,
               negozi_barrafranca.email_negozio,
               negozi_barrafranca.orario_negozio,
               negozi_barrafranca.giorno_chiusura_negozio,
               negozi_barrafranca.categoria_negozio,
               offerte_barrafranca.link_pdf
        FROM  negozi_barrafranca
        LEFT JOIN offerte_barrafranca ON negozi_barrafranca.nome_negozio = offerte_barrafranca.nome_negozio";

$result = mysqli_query($conn, $sql);

if (!$result) {
  echo "NESSUNRISULTATOBARRAFRANCACONNECTPHPSCRIPT ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
  if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $rows = array();
    while($r = mysqli_fetch_assoc($result)) {
      $rows[] = $r;
    }
    print json_encode($rows);
    exit;
  } else {
      echo "NESSUNRISULTATOBARRAFRANCACONNECTPHPSCRIPT";
  }

}

mysqli_close($conn);

 ?>
