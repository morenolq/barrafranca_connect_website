<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_lavoro'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_work = mysqli_real_escape_string($conn, $_GET['nome_lavoro']);

if (isEmpty($name_transfer_work)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM nuovi_lavori_barrafranca WHERE nome_lavoro='".$name_transfer_work."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Lavoro non trovato.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "INSERT INTO lavori_barrafranca (nome_lavoro, nome_contatto_lavoro, email_lavoro, telefono_lavoro, indirizzo_lavoro, descrizione_lavoro, come_contattarti)
  SELECT * FROM nuovi_lavori_barrafranca WHERE nome_lavoro ='".$name_transfer_work."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore inserimento lavoro ".$name_transfer_work.", riprova più tardi.");
  }

  $sql = "DELETE FROM nuovi_lavori_barrafranca WHERE nome_lavoro ='".$name_transfer_work."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore delete lavoro ".$name_transfer_work.", riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore commit lavoro ".$name_transfer_work.", riprova più tardi.");
  } else {
    mysqli_close($conn);
    goBackAdminActionScript("lavoro ".$name_transfer_work." trasferito correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goBackAdminActionScript($e->getMessage());
  exit;
}

?>
