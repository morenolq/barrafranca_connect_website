<?php

session_start();
include("./utility_php_bc.php");

if(!isset($_SESSION['username']) || !isset($_SESSION['nome_creatore']) || !isset($_SESSION['role']) ){
  returnHomeScript("Errore interno, prova a rifare il login");
  exit;
}

if($_SESSION["role"]!=="CREATOR"){
  returnHomeScript("Errore, Non hai i diritti per questa funzionalità.");
  exit;
}

$errors = array(); //if any error
$uploadedFiles = array();
$extension = array("jpeg","jpg","png","gif"); //admitted extension
$bytes = 1024;
$KB = 2024;
$totalBytes = $bytes * $KB; //max file size 2MB
$folder_name = removeAccents($_POST["nome_album"]);
$UploadFolder = "../PHOTO_ALBUM/" . $folder_name;

if (!mkdir($UploadFolder, 0777)) {
  goToCreatorHomeScript("Si è verificato un errore durante il caricamento delle immagini, probabilmente un album con questo nome esiste già, riprova.");
  exit;
}

$counter = 0;

foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
  $temp = $_FILES["files"]["tmp_name"][$key];
  $name = $_FILES["files"]["name"][$key];

  if(empty($temp)){
    break;
  }

  $counter++;

  if($_FILES["files"]["size"][$key] > $totalBytes){
    goToCreatorHomeScript("Errore, almeno una delle immagini inviate ha una dimensione superiore a 1 MB.");
    exit;
  }

  $ext = pathinfo($name, PATHINFO_EXTENSION);
  if(in_array($ext, $extension) == false){
    goToCreatorHomeScript("Errore, almeno uno dei file inviati non è un'immagine supportata (jpg, jpeg, gif, png).");
    exit;
  }

  if(!move_uploaded_file($temp,$UploadFolder."/".$counter.".".$ext)){
    goToCreatorHomeScript("Si è verificato un errore durante il caricamento delle immagini, probabilmente un album con questo nome esiste già, riprova.");
    exit;
  }
  array_push($uploadedFiles, $counter.".".$ext);
}

if($counter>0){
  if(count($errors)>0){
    goToCreatorHomeScript("Si è verificato un errore durante il caricamento delle immagini, riprova.");
  }

  $conn = getConnection();
  if (!$conn) {
    goToCreatorHomeScript("C'è stato un problema nei nostri server, ci scusiamo per l'inconveniente, riprovare più tardi.");
    exit;
  }

  mysqli_query($conn, "SET NAMES 'utf8'");
  mysql_set_charset("utf8");

  try {
    mysqli_autocommit($conn, false);
    $album_name = mysqli_real_escape_string($conn, $_POST["nome_album"]);
    $name_creator = mysqli_real_escape_string($conn, $_SESSION['nome_creatore']);
    $descrizione_album = mysqli_real_escape_string($conn, $_POST['descrizione_album']);
    $link_creatore = mysqli_real_escape_string($conn, $_POST['link_creatore']);

    $sql = "SELECT * FROM album_barrafranca WHERE nome_album = '".$album_name."';";

    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
      throw new Exception("Errore, un altro album con lo stesso nome esiste già: ".$album_name.". Riprova inserendo un altro nome.");
    }

    $sql = "INSERT INTO foto_barrafranca VALUES ";

    foreach($uploadedFiles as $fileName){
      $sql = $sql . "('".$album_name."', 'www.barrafrancaconnect.com/PHOTO_ALBUM/".$folder_name."/".$fileName."'),";
    }

    $sql = substr($sql, 0, -1);

    if (mysqli_query($conn, $sql)) {
      $sql = "INSERT INTO album_barrafranca VALUES ('".$album_name."', '".$name_creator."', '".$descrizione_album."', '".$link_creatore."')";

      if (mysqli_query($conn, $sql)) {
        //OK
      } else {
        throw new Exception("Errore durante la creazione dell'album ".$album_name.". Riprova più tardi.");
      }

      if (!mysqli_commit($conn)){
        throw new Exception("Errore durante la creazione dell'album ".$album_name.". Riprova più tardi.");
      } else {
        mysqli_close($conn);
        goToCreatorHomeScript("Album caricato correttamente.");
        exit;
      }

    } else {
      mysqli_close($conn);
      throw new Exception("Errore durante il caricamento. Riprova.");
      exit;
    }

  } catch (Exception $e){
    mysqli_rollback($conn);
    goToCreatorHomeScript($e->getMessage());
    exit;
  }

}
else{
  goToCreatorHomeScript("Errore durante il caricamento. Riprova.");
  exit;
}

exit;

 ?>
