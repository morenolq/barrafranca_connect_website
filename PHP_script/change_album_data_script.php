<?php

include("./utility_php_bc.php");
session_start();

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHomeScript("Errore di connessione con il Database");
}


if (!isset($_POST["nome_album"]) ||
    !isset($_POST["nome_creatore"]) ||
    !isset($_POST["descrizione_album"]) ||
    !isset($_POST["link_creatore"])
    ){
  goToCreatorHomeScript("Errore nell'invio del form per la modifica delle informazioni, riprovare.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if ($_POST["nome_creatore"] !== $_SESSION["nome_creatore"]){
  goToCreatorHomeScript("Errore, non hai il diritto di modificare queste informazioni.");
}

$nome_creatore = mysqli_real_escape_string($conn, $_POST["nome_creatore"]);
$nome_album = mysqli_real_escape_string($conn, $_POST["nome_album"]);
$descrizione_album = mysqli_real_escape_string($conn, $_POST["descrizione_album"]);
$link_creatore = mysqli_real_escape_string($conn, $_POST["link_creatore"]);

if (isEmpty($nome_creatore) ||
    isEmpty($nome_album) ||
    isEmpty($descrizione_album) ||
    isEmpty($link_creatore)
  ){
  goToCreatorHomeScript("Errore nell'invio del form per la modifica delle informazioni, riprovare.");
  exit;
}

try {
  mysqli_autocommit($conn, false);
  $sql = "UPDATE album_barrafranca
          SET descrizione_album = '".$descrizione_album."' ,
              link_creatore = '".$link_creatore."'
          WHERE nome_album = '".$_POST["nome_album"]."';";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore durante l'aggiornamento delle informazioni, riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore durante l'aggiornamento delle informazioni, riprova più tardi.");
  } else {
    mysqli_close($conn);
    goToCreatorHomeScript("Informazioni cambiate con successo.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goToCreatorHomeScript($e->getMessage());
  exit;
}

?>
