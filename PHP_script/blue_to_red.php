<?php

include("./utility_php_bc.php");

// Create connection
$conn = getConnection();

//print_r($_POST);
//exit;

// Check connection
if (!$conn) {
  returnHomeScript("Errore di connessione con il Database");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["nome_negozio"]) ||
    !isset($_POST["email_account_red"]) ||
    !isset($_POST["password_account_red1"]) ||
    !isset($_POST["password_account_red2"])) {
      returnHomeScript("Errore, campi mancanti.");
      exit;
    }

$nome_negozio = mysqli_real_escape_string($conn, $_POST["nome_negozio"]);
$email_account = mysqli_real_escape_string($conn, $_POST["email_account_red"]);
$password_account_red1 = mysqli_real_escape_string($conn, $_POST["password_account_red1"]);
$password_account_red2 = mysqli_real_escape_string($conn, $_POST["password_account_red2"]);

if (isEmpty($nome_negozio) || isEmpty($email_account) || isEmpty($password_account_red1) || isEmpty($password_account_red2)){
  returnHomeScript("Errore, campi mancanti.");
  exit;
}

if (!isEmail($email_account)){
  returnHomeScript("Errore, email non valida.");
  exit;
}

if ($password_account_red1 !== $password_account_red2){
  returnHomeScript("Errore, password diverse, riprovare.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "SELECT * FROM negozi_barrafranca WHERE nome_negozio = '".$nome_negozio."' AND user_red = false";


  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore interno, riprova più tardi.");
  }

  if (mysqli_num_rows($result) <= 0){
    throw new Exception("Errore, negozio non esistente o account Rosso già presente.");
  }


  $sql = "UPDATE negozi_barrafranca SET user_red = true WHERE nome_negozio = '".$nome_negozio."'";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore inserimento, riprova più tardi.");
  }

  $md5_password = md5($password_account_red1);
  $data_pagamento = date('Y-m-d', strtotime("+90 day"));
  $sql = "INSERT INTO login_red (nome_utente, password, data_pagamento, nome_negozio)
  VALUES ('".$email_account."', '".$md5_password."', '".$data_pagamento."', '".$nome_negozio."')";

  if (!($result = mysqli_query($conn, $sql))){
    mysqli_rollback($conn);
    throw new Exception("Errore inserimento, riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore inserimento, riprova più tardi.");
  } else {
    mysqli_close($conn);
    $body = "Salve, la registrazione a Barrafranca Connect è avvenuta con successo<br><br>
                   Le credenziali sono:<br>
                   Nome Utente: ".$email_account."<br>
                   Password: ".$password_account_red1."<br><br>
                   Grazie per la registrazione,<br>
                   Il Team di Barrafranca Connect";

    $body2 = "Salve, Passaggio BLU to RED a Barrafranca Connect è avvenuta con successo<br><br>
                Le credenziali sono:<br>
                Nome Utente: ".$email_account."<br><br>
                Grazie per la registrazione,<br>
                Il Team di Barrafranca Connect";

    if (sendEmail($email_account, "Registrazione Barrafranca Connect", $body)){
      sendEmail("amministrazione@barrafrancaconnect.com", "Passaggio BLU to RED Barrafranca Connect", $body2);
      returnHomeScript("Registrazione avvenuta con successo. Riceverai un'email di conferma.");
      exit;
    } else {
      returnHomeScript("Registrazione avvenuta con successo.");
      exit;
    }

  }

} catch (Exception $e){
  mysqli_rollback($conn);
  returnHomeScript($e->getMessage());
  exit;
}





?>
