<?php

session_start();
include("./utility_php_bc.php");

if (!isset($_SESSION["role"])){
  returnHomeScript("Errore, rifare il login.");
  exit;
}

if ($_SESSION["role"]!=="USER_RED"){
  returnHomeScript("Errore, permesso non consentito.");
  exit;
}

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHomeScript("Errore di connessione con il Database");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["nome_evento"]) ||
    !isset($_POST["email_evento"]) ||
    !isset($_POST["telefono_evento"]) ||
    !isset($_POST["data_evento"]) ||
    !isset($_POST["ora_evento"]) ||
    !isset($_POST["nome_organizzatore_evento"]) ||
    !isset($_POST["descrizione_evento"]) ||
    !isset($_POST["come_contattarti"]) ||
    !isset($_SESSION["username"])
    ){
  returnHomeScript("Errore nell'invio del form, riprovare.");
  exit;
}

$nome_evento = mysqli_real_escape_string($conn, $_POST["nome_evento"]);
$email_evento = mysqli_real_escape_string($conn, $_POST["email_evento"]);
$telefono_evento = mysqli_real_escape_string($conn, $_POST["telefono_evento"]);
$data_evento = mysqli_real_escape_string($conn, $_POST["data_evento"]);
$ora_evento = mysqli_real_escape_string($conn, $_POST["ora_evento"]);
$nome_organizzatore_evento = mysqli_real_escape_string($conn, $_POST["nome_organizzatore_evento"]);
$descrizione_evento = mysqli_real_escape_string($conn, $_POST["descrizione_evento"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);
$email_inserzionista = mysqli_real_escape_string($conn, $_SESSION["username"]);

if (isEmpty($nome_evento) ||
    isEmpty($email_evento) ||
    isEmpty($telefono_evento) ||
    isEmpty($data_evento) ||
    isEmpty($ora_evento) ||
    isEmpty($nome_organizzatore_evento) ||
    isEmpty($descrizione_evento) ||
    isEmpty($come_contattarti) ||
    isEmpty($email_inserzionista)
  ){
  goToUserRedHomeScript("Errore nell'invio del form, riprovare.");
  exit;
}

if (!isEmail($email_inserzionista) || !isEmail($email_evento)){
  goToUserRedHomeScript("Errore, email non valida.");
  exit;
}

if (!isset($_FILES['PDFFile']) || !isset($_FILES['PDFFile']['tmp_name']) || !isset($_FILES['PDFFile']['name']) || !is_uploaded_file($_FILES['PDFFile']['tmp_name'])) {
  //NO FILE EVENT
  $sql = "INSERT INTO eventi_barrafranca (nome_evento, data_evento, ora_evento, email_evento, telefono_evento, nome_organizzatore_evento, descrizione_evento, link_pdf, come_contattarti, email_inserzionista)
  VALUES ('".$nome_evento."', '".$data_evento."', '".$ora_evento."', '".$email_evento."', '".$telefono_evento."', '".$nome_organizzatore_evento."', '".$descrizione_evento."', null, '".$come_contattarti."', '".$email_inserzionista."')";
  if (mysqli_query($conn, $sql)) {
      mysqli_close($conn);
      goToUserRedHomeScript("Evento inserito correttamente! grazie.");
      exit;
  } else {
      mysqli_close($conn);
      goToUserRedHomeScript("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
      exit;
  }
} else {

  $userfile_tmp = mysqli_real_escape_string($conn, $_FILES['PDFFile']['tmp_name']);
  $userfile_name = mysqli_real_escape_string($conn, $_FILES['PDFFile']['name']);
  $ext = pathinfo($userfile_name, PATHINFO_EXTENSION);
  if ($ext !== "pdf"){
    goToUserRedHomeScript("Il file ricevuto non è un pdf, riprovare.");
    exit;
  }
  $file_name = removeAccents("../PDF_OFFERS/LOCANDINA_EVENTO_".strtr($_SESSION['nome_negozio'], array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);
  if (move_uploaded_file($userfile_tmp, $file_name)) {
    $link_pdf = removeAccents("./PDF_OFFERS/LOCANDINA_EVENTO_".strtr($_SESSION['nome_negozio'], array('.' => '', ',' => ''))."_".date("d_m_y_H_i_s").".".$ext);
    $sql = "INSERT INTO eventi_barrafranca (nome_evento, data_evento, ora_evento, email_evento, telefono_evento, nome_organizzatore_evento, descrizione_evento, link_pdf, come_contattarti, email_inserzionista)
    VALUES ('".$nome_evento."', '".$data_evento."', '".$ora_evento."', '".$email_evento."', '".$telefono_evento."', '".$nome_organizzatore_evento."', '".$descrizione_evento."', '".$link_pdf."', '".$come_contattarti."', '".$email_inserzionista."')";
    if (mysqli_query($conn, $sql)) {
        mysqli_close($conn);
        goToUserRedHomeScript("Evento inserito correttamente! grazie.");
        exit;
    } else {
        mysqli_close($conn);
        goToUserRedHomeScript("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
        exit;
    }

    mysqli_close($conn);
  } else {
    goToUserRedHomeScript("Errore nel caricamento del file, riprovare.");
    exit;
  }

}




?>
