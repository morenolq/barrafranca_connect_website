<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_GET['nome_consiglio'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$name_transfer_delete = mysqli_real_escape_string($conn, $_GET['nome_consiglio']);
$sql = "SELECT * FROM avvisi_barrafranca WHERE nome_consiglio='".$name_transfer_delete."';";
$result = mysqli_query($conn, $sql);

if (isEmpty($name_transfer_delete)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Consiglio non trovato.");
  exit;
}

$sql = "DELETE FROM avvisi_barrafranca WHERE nome_consiglio ='".$name_transfer_delete."';";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Consiglio " . $name_transfer_delete . " cancellato correttamente");
  exit;
} else {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Attenzione errore nella cancellazione di " . $name_transfer_delete);
  exit;
}

mysqli_close($conn);

?>
