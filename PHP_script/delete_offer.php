<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_offerta'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_offer = mysqli_real_escape_string($conn, $_GET['nome_offerta']);

if (isEmpty($name_transfer_offer)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM offerte_barrafranca WHERE nome_offerta='".$name_transfer_offer."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Offerta non trovata.");
  exit;
}

$sql = "DELETE FROM offerte_barrafranca WHERE nome_offerta ='".$name_transfer_offer."';";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Offerta " . $name_transfer_offer . " eliminata correttamente");
  exit;
} else {
  mysqli_close($conn);
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Attenzione errore nella cancellazione di " . $name_transfer_offer);
  exit;
}

mysqli_close($conn);


?>
