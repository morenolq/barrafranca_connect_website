<?php
session_start();
include("utility_php_bc.php");
// Create connection
$conn = getConnection();
// Check connection


if (!$conn) {
  returnHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

if (!isset($_POST["current_password"]) ||
    !isset($_POST["new_password_1"]) ||
    !isset($_POST["new_password_2"]) ||
    !isset($_SESSION["role"]) ||
    !isset($_SESSION["username"])){
  returnHomeScript("Errore nell'invio del form per il recupero password");
  exit;
}

$current_password = mysqlCleaner($conn, $_POST["current_password"]);
$new_password_1 = mysqlCleaner($conn, $_POST["new_password_1"]);
$new_password_2 = mysqlCleaner($conn, $_POST["new_password_2"]);
$type_login = mysqlCleaner($conn, $_SESSION["role"]);
$username_user = mysqlCleaner($conn, $_SESSION["username"]);

if (isEmpty($current_password) ||
    isEmpty($new_password_1) ||
    isEmpty($new_password_2) ||
    isEmpty($type_login) ||
    isEmpty($username_user)){
  returnHomeScript("Errore, campi mancanti.");
  exit;
}

if (!isEmail($username_user)){
  returnHomeScript("Errore, rieffettua l'accesso prima di cambiare la password.");
  exit;
}

if ($type_login==="USER_RED"){
  $sql = "SELECT password FROM login_red WHERE nome_utente='".$username_user."'";
} else if($type_login==="ADMINISTRATOR"){
  $sql = "SELECT password FROM login_pc WHERE nome_utente='".$username_user."'";
} else if($type_login==="CREATOR"){
  $sql = "SELECT password_creatore FROM login_creators WHERE email_creatore='".$username_user."'";
} else if($type_login==="MUNICIPALITY"){
  $sql = "SELECT password FROM login_comune WHERE nome_utente='".$username_user."'";
} else {
  returnHomeScript("Si è verificato un errore nel recupero password, riprova.");
  exit;
}

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  //ok
} else {
  returnHomeScript("Errore, utente non trovato.");
  exit;
}

$row=mysqli_fetch_array($result);
$old_password_db = $row[0];

if ($old_password_db !== md5($current_password)){
  returnHomeScript("Errore, password errata.");
  exit;
}

if ($new_password_1 !== $new_password_2){
  returnHomeScript("Le due password non coincidono");
  exit;
}

$new_password = md5($new_password_1);

if ($type_login==="USER_RED"){
  //$sql = "UPDATE login_red SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_red SET password = '".$new_password."' WHERE nome_utente='".$username_user."'";
} else if($type_login==="ADMINISTRATOR"){
  //$sql = "UPDATE login_pc SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_pc SET password = '".$new_password."' WHERE nome_utente='".$username_user."'";
} else if($type_login==="CREATOR"){
  //$sql = "UPDATE login_pc SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_creators SET password_creatore = '".$new_password."' WHERE email_creatore='".$username_user."'";
} else if($type_login==="MUNICIPALITY"){
  //$sql = "UPDATE login_pc SET password='$temp_psw_md5' WHERE nome_utente='$username_user'";
  $sql = "UPDATE login_comune SET password = '".$new_password."' WHERE nome_utente='".$username_user."'";
} else {
  returnHomeScript("Si è verificato un errore nel recupero password, riprova.");
  exit;
}

$result = mysqli_query($conn, $sql);

if ($result){
  mysqli_close($conn);
  //send email

  $body = "Salve, è stata cambiata la password di Barrafranca Connect<br><br>
           Le nuove credenziali sono:<br>
           Nome Utente: ".$username_user."<br>
           Password: ".$new_password_1."<br><br>
           Grazie,<br>
           Il Team di Barrafranca Connect";

   if (sendEmail($username_user, "Reset Password Barrafranca Connect", $body)){
     returnHomeScript("Password cambiata correttamente. Riceverai un'email di conferma.");
     exit;
   } else {
     returnHomeScript("Password cambiata correttamente.");
     exit;
   }

} else {
  mysqli_close($conn);
  returnHomeScript("Errore, non è stato possibile cambiare la password");
  exit;
}

mysqli_close($conn);

 ?>
