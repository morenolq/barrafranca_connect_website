<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="USER_RED"){
  returnHomeScript("Errore, non hai il diritto di entrare in questa sezione del sito.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goToUserRedHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_evento'])){
  goToUserRedHomeScript("Errore nell'invio della richiesta");
  exit;
}

if (!isset($_SESSION["username"])){
  goToUserRedHomeScript("Errore nell'invio della richiesta, rifare l'accesso");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_event = mysqli_real_escape_string($conn, $_GET["nome_evento"]);
$email_inserzionista = mysqli_real_escape_string($conn, $_SESSION["username"]);


if (isEmpty($name_transfer_event) || isEmpty($email_inserzionista)){
  goToUserRedHomeScript("Errore nell'invio della richiesta");
  exit;
}

if(!isEmail($email_inserzionista)){
  goToUserRedHomeScript("Errore nell'invio della richiesta, rifare l'accesso");
  exit;
}

$sql = "SELECT * FROM eventi_barrafranca WHERE nome_evento='".$name_transfer_event."' AND email_inserzionista='".$email_inserzionista."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
    goToUserRedHomeScript("Errore: Evento non trovato.");
    exit;
}


$sql = "DELETE FROM eventi_barrafranca WHERE nome_evento ='".$name_transfer_event."' AND email_inserzionista='".$email_inserzionista."';;";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goToUserRedHomeScript("Evento " . $name_transfer_event . " eliminato correttamente");
  exit;
} else {
  mysqli_close($conn);
  goToUserRedHomeScript("Errore nella cancellazione di " . $name_transfer_event);
  exit;
}

mysqli_close($conn);

//USAGE http://mlqlab.co.nf/BO_SERVER/SCRIPT_DB/move_event.php?nome_evento=QUIILNOME

?>
