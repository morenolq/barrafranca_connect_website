<?php
session_start();
include("./utility_php_bc.php");

if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

// Create connection
$conn = getConnection();

if (!isset($_GET["nome_farmacia"])){
  goToAdminHomeScript("Errore, non cambiare a mano l'URL, riprova");
  exit;
}

// Check connection
if (!$conn) {
  returnHome("Errore di connessione con il Database");
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_farmacia = mysqli_real_escape_string($conn, $_GET["nome_farmacia"]);

if (isEmpty($nome_farmacia)){
  goToAdminHomeScript("Errore, non cambiare a mano l'URL, riprova");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "UPDATE farmacie_barrafranca SET diturno_farmacia = false";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore nella query UP1, riprova.");
  }

  $sql = "UPDATE farmacie_barrafranca SET diturno_farmacia = true WHERE nome_farmacia = '".$nome_farmacia."'";

  if (!($result = mysqli_query($conn, $sql))){
    mysqli_rollback($conn);
    throw new Exception("Errore nella query UP2, riprova.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore Commit");
  } else {
    mysqli_close($conn);
    goToAdminHomeScript("Farmacia di turno cambiata correttamente, nuova farmacia di turno: " . $nome_farmacia);
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goToAdminHomeScript($e->getMessage());
  exit;
}

?>
