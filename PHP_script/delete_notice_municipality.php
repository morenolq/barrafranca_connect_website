<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="MUNICIPALITY"){
  returnHomeScript("Errore, non hai il diritto di entrare in questa sezione del sito.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goToMunicipalityHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_avviso'])){
  goToMunicipalityHomeScript("Errore nell'invio della richiesta");
  exit;
}

if (!isset($_SESSION["username"])){
  goToMunicipalityHomeScript("Errore nell'invio della richiesta, rifare l'accesso");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_notice = mysqli_real_escape_string($conn, $_GET["nome_avviso"]);
$email_inserzionista = mysqli_real_escape_string($conn, $_SESSION["username"]);


if (isEmpty($name_transfer_notice) || isEmpty($email_inserzionista)){
  goToMunicipalityHomeScript("Errore nell'invio della richiesta");
  exit;
}

if(!isEmail($email_inserzionista)){
  goToMunicipalityHomeScript("Errore nell'invio della richiesta, rifare l'accesso");
  exit;
}

$sql = "SELECT * FROM avvisi_amministrazione_barrafranca WHERE nome_avviso='".$name_transfer_notice."' AND email='".$email_inserzionista."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
    goToMunicipalityHomeScript("Errore: Evento non trovato.");
    exit;
}

$sql = "DELETE FROM avvisi_amministrazione_barrafranca WHERE nome_avviso ='".$name_transfer_notice."' AND email = '".$email_inserzionista."';;";

if (mysqli_query($conn, $sql)) {
  mysqli_close($conn);
  goToMunicipalityHomeScript("Avviso " . $name_transfer_notice . " eliminato correttamente");
  exit;
} else {
  mysqli_close($conn);
  goToMunicipalityHomeScript("Errore nella cancellazione di " . $name_transfer_notice);
  exit;
}

mysqli_close($conn);

?>
