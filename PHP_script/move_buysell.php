<?php

session_start();
include("./utility_php_bc.php");

if (!isset($_SESSION["role"])){
  returnHomeScript("Errore, rifare il login.");
}

if ($_SESSION["role"]!=="ADMINISTRATOR"){
  returnHomeScript("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_comprovendo'])){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");
$name_transfer_buysell = mysqli_real_escape_string($conn, $_GET['nome_comprovendo']);

if (isEmpty($name_transfer_buysell)){
  goToAdminHomeScript("Errore nessun campo GET");
  exit;
}

$sql = "SELECT * FROM nuovi_comprovendo_barrafranca WHERE nome_comprovendo='".$name_transfer_buysell."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goBackAdminActionScript("messageAdminActionBarrafrancaConnect", "Errore: Compro Vendo non trovato.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "INSERT INTO comprovendo_barrafranca (nome_comprovendo, prezzo_comprovendo, nome_contatto_comprovendo, email_comprovendo, telefono_comprovendo, descrizione_comprovendo, come_contattarti)
  SELECT * FROM nuovi_comprovendo_barrafranca WHERE nome_comprovendo ='".$name_transfer_buysell."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore inserimento compro vendo ".$name_transfer_buysell.", riprova più tardi.");
  }

  $sql = "DELETE FROM nuovi_comprovendo_barrafranca WHERE nome_comprovendo ='".$name_transfer_buysell."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore delete Compro Vendo ".$name_transfer_buysell.", riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore commit Compro Vendo ".$name_transfer_buysell.", riprova più tardi.");
  } else {
    mysqli_close($conn);
    goBackAdminActionScript("Compro Vendo ".$name_transfer_buysell." trasferito correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goBackAdminActionScript($e->getMessage());
  exit;
}

?>
