<?php

include("./utility_php_bc.php");

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  //echo "ERRORE DATABASE";
  echo "Si è verificato un errore temporaneo, riprova più tardi";
}

if (!isset($_POST["nome_negozio"]) ||
    !isset($_POST["indirizzo_negozio"]) ||
    !isset($_POST["email_negozio"]) ||
    !isset($_POST["telefono_negozio"]) ||
    !isset($_POST["descrizione_negozio"]) ||
    !isset($_POST["categoria_negozio"]) ||
    !isset($_POST["come_contattarti"]) ||
    !isset($_POST["email_profilo_negozio"]) ||
    !isset($_POST["password1"]) ||
    !isset($_POST["password2"])
    ){
  //echo "ERRORE MANCANO PARAMETRI POST";
  echo "Si è verificato un errore nei dati, controlla e riprova";
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_negozio = mysqli_real_escape_string($conn, $_POST["nome_negozio"]);
$indirizzo_negozio = mysqli_real_escape_string($conn, $_POST["indirizzo_negozio"]);
$email_negozio = mysqli_real_escape_string($conn, $_POST["email_negozio"]);
$telefono_negozio = mysqli_real_escape_string($conn, $_POST["telefono_negozio"]);
$descrizione_negozio = mysqli_real_escape_string($conn, $_POST["descrizione_negozio"]);
$orario_negozio = mysqli_real_escape_string($conn, $_POST["orario_negozio"]);
$giorno_chiusura_negozio = mysqli_real_escape_string($conn, $_POST["giorno_chiusura_negozio"]);
$categoria_negozio = mysqli_real_escape_string($conn, $_POST["categoria_negozio"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);
$nome_utente = mysqli_real_escape_string($conn, $_POST["email_profilo_negozio"]);
$password_account_red1 = mysqli_real_escape_string($conn, $_POST["password1"]);
$password_account_red2 = mysqli_real_escape_string($conn, $_POST["password2"]);

if (isEmpty($nome_negozio) ||
    isEmpty($email_negozio) ||
    isEmpty($telefono_negozio) ||
    isEmpty($indirizzo_negozio) ||
    isEmpty($categoria_negozio) ||
    isEmpty($descrizione_negozio) ||
    isEmpty($come_contattarti) ||
    isEmpty($nome_utente) ||
    isEmpty($password_account_red1) ||
    isEmpty($password_account_red2)
  ){
  //echo "ERRORE MANCANO PARAMETRI";
  echo "Si è verificato un errore nei dati, controlla e riprova";
  exit;
}

if ($password_account_red1 !== $password_account_red2){
  //echo "ERRORE PASSWORD DIVERSE";
  echo "Errore, le password non coincidono, riprova";
  exit;
}

if (!isEmail($nome_utente)){
  //echo "ERRORE EMAIL NON VALIDA";
  echo "Errore, l'email inserita non è valida";
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "SELECT * FROM negozi_barrafranca WHERE nome_negozio = '".$nome_negozio."'";

  if (!($result = mysqli_query($conn, $sql))){
    //echo "ERRORE INTERNO";
    echo "Si è verificato un errore temporaneo, riprova più tardi";
    exit;
  }

  if (mysqli_num_rows($result) > 0){
    //throw new Exception("ERRORE NEGOZIO ESISTENTE");
    throw new Exception("Errore, il negozio esiste già. Fai il login su www.barrafrancaconnect.com");
  }

  $sql = "INSERT INTO negozi_barrafranca (nome_negozio, indirizzo_negozio, email_negozio, telefono_negozio, categoria_negozio, giorno_chiusura_negozio, orario_negozio, descrizione_negozio, come_contattarti, user_red)
  VALUES ('".$nome_negozio."', '".$indirizzo_negozio."', '".$email_negozio."', '".$telefono_negozio."', '".$categoria_negozio."', '".$giorno_chiusura_negozio."', '".$orario_negozio."', '".$descrizione_negozio."', '".$come_contattarti."', true)";

  if (!($result = mysqli_query($conn, $sql))){
    //throw new Exception("ERRORE INSERIMENTO");
    throw new Exception("Si è verificato un errore temporaneo, riprova più tardi");
  }

  $md5_password = md5($password_account_red1);
  $data_pagamento = date('Y-m-d', strtotime("+90 day"));
  $sql = "INSERT INTO login_red (nome_utente, password, data_pagamento, nome_negozio)
  VALUES ('".$nome_utente."', '".$md5_password."', '".$data_pagamento."', '".$nome_negozio."')";

  if (!($result = mysqli_query($conn, $sql))){
    mysqli_rollback($conn);
    //throw new Exception("ERRORE INSERIMENTO EMAIL GIA REGISTRATA");
    throw new Exception("Errore, questa email ha già un account. Fai il login su www.barrafrancaconnect.com");
  }

  if (!mysqli_commit($conn)){
    //throw new Exception("ERRORE COMMIT");
    throw new Exception("Si è verificato un errore temporaneo, riprova più tardi");
  } else {
    mysqli_close($conn);
    $body = "Salve, la registrazione a Barrafranca Connect è avvenuta con successo<br><br>
             Le credenziali sono:<br>
             Nome Utente: ".$nome_utente."<br>
             Password: ".$password_account_red1."<br><br>
             Grazie per la registrazione,<br>
             Il Team di Barrafranca Connect";

     $body2 = "Salve, Registrazione nuovo USER RED<br><br>
              Le credenziali sono:<br>
              Nome Utente: ".$nome_utente."<br><br>
              Grazie per la registrazione,<br>
              Il Team di Barrafranca Connect";

    if (sendEmail($nome_utente, "Registrazione Barrafranca Connect", $body)){
      sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO USER RED Barrafranca Connect", $body2);
      //echo "OK"
      echo "Il negozio è stato registrato con successo, riceverai un'email di conferma. Puoi fare il login su www.barrafrancaconnect.com per inserire Eventi, Volantini e modificare i tuoi dati.";
      exit;
    } else {
      //echo "OK NO MAIL";
      echo "Il negozio è stato registrato con successo, puoi fare il login su www.barrafrancaconnect.com per inserire Eventi, Volantini e modificare i tuoi dati.";
      exit;
    }

  }

} catch (Exception $e){
  mysqli_rollback($conn);
  echo $e->getMessage();
  exit;
}

?>
