<?php

session_start();
include("./utility_php_bc.php");
if ($_SESSION["role"]!=="CREATOR"){
  returnHomeScript("Errore, non hai il diritto di modificare queste informazioni.");
  exit;
}

//[Connection + Check]
$conn = getConnection();
if (!$conn) {
  goToCreatorHomeScript("Si è verificato un errore nella parte server, riprova più tardi.");
  exit;
}

if (!isset($_GET['nome_album'])){
  goToCreatorHomeScript("Errore, non modificare l'url, riprova.");
  exit;
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_album = mysqli_real_escape_string($conn, $_GET['nome_album']);
$nome_creatore = mysqli_real_escape_string($conn, $_SESSION['nome_creatore']);

if (isEmpty($nome_album)){
  goToCreatorHomeScript("Errore, non modificare l'url, riprova.");
  exit;
}

$sql = "SELECT * FROM album_barrafranca WHERE nome_album='".$nome_album."' AND nome_creatore='".$nome_creatore."';";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) <= 0) {
  goToCreatorHomeScript("Errore: album non trovato.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "DELETE FROM album_barrafranca WHERE nome_album ='".$nome_album."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore cancellazione album ".$nome_album.". Riprova più tardi.");
  }

  $sql = "DELETE FROM foto_barrafranca WHERE nome_album ='".$nome_album."';";

  if (mysqli_query($conn, $sql)) {
    //OK
  } else {
    throw new Exception("Errore cancellazione album ".$nome_album.". Riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore cancellazione album ".$nome_album.". Riprova più tardi.");
  } else {
    mysqli_close($conn);
    Delete("../PHOTO_ALBUM/".$nome_album);
    goToCreatorHomeScript("Album ".$nome_album." cancellato correttamente.");
    exit;
  }

} catch (Exception $e){
  mysqli_rollback($conn);
  goToCreatorHomeScript($e->getMessage());
  exit;
}

?>
