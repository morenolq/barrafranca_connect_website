<?php

include("./utility_php_bc.php");

// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
  returnHome("Errore di connessione con il Database");
}

if (!isset($_POST["nome_creator"]) ||
    !isset($_POST["email_account_creator"]) ||
    !isset($_POST["password_account_creator1"]) ||
    !isset($_POST["password_account_creator2"])
    ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}


mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_creator = mysqli_real_escape_string($conn, $_POST["nome_creator"]);
$email_account_creator = mysqli_real_escape_string($conn, $_POST["email_account_creator"]);
$password_account_creator1 = mysqli_real_escape_string($conn, $_POST["password_account_creator1"]);
$password_account_creator2 = mysqli_real_escape_string($conn, $_POST["password_account_creator2"]);

if (isEmpty($nome_creator) ||
    isEmpty($email_account_creator) ||
    isEmpty($password_account_creator1) ||
    isEmpty($password_account_creator2)
  ){
  returnHomeScript("Errore nell'invio del form per la registrazione, riprovare.");
  exit;
}

if ($password_account_creator1 !== $password_account_creator2){
  returnHomeScript("Errore, password diverse, riprovare.");
  exit;
}

if (!isEmail($email_account_creator)){
  returnHomeScript("Errore, email non valida.");
  exit;
}

try {
  mysqli_autocommit($conn, false);

  $sql = "SELECT * FROM login_creators WHERE email_creatore = '".$email_account_creator."'";

  if (!($result = mysqli_query($conn, $sql))){
    throw new Exception("Errore interno, riprova più tardi");
    exit;
  }

  if (mysqli_num_rows($result) > 0){
    throw new Exception("Errore, Account gia' registrato");
  }

  $md5_password = md5($password_account_creator1);
  $sql = "INSERT INTO login_creators (email_creatore, password_creatore, nome_creatore)
  VALUES ('".$email_account_creator."', '".$md5_password."', '".$nome_creator."')";

  if (!($result = mysqli_query($conn, $sql))){
    mysqli_rollback($conn);
    throw new Exception("Errore inserimento, probabilmente un account con questa email esiste già, se non è così riprova più tardi.");
  }

  if (!mysqli_commit($conn)){
    throw new Exception("Errore inserimento, riprova più tardi.");
  } else {
    mysqli_close($conn);
    $body = "Salve, la registrazione a Barrafranca Connect è avvenuta con successo<br><br>
             Le credenziali sono:<br>
             Nome Utente: ".$email_account_creator."<br>
             Password: ".$password_account_creator1."<br><br>
             Grazie per la registrazione,<br>
             Il Team di Barrafranca Connect";

     $body2 = "Salve, Registrazione nuovo CREATOR<br><br>
              Le credenziali sono:<br>
              Nome Utente: ".$email_account_creator."<br><br>
              Grazie per la registrazione,<br>
              Il Team di Barrafranca Connect";

    if (sendEmail($email_account_creator, "Registrazione Barrafranca Connect", $body)){
      sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO CREATOR Barrafranca Connect", $body2);
      returnHomeScript("Registrazione avvenuta con successo. Riceverai un'email di conferma.");
      exit;
    } else {
      returnHomeScript("Registrazione avvenuta con successo.");
      exit;
    }

  }

} catch (Exception $e){
  mysqli_rollback($conn);
  returnHomeScript($e->getMessage());
  exit;
}

?>
