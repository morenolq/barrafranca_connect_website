<?php
include ('./PHP_script/utility_php_bc.php');
session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>

  function validateMigrateForm(){

    nome_negozio = document.getElementById("nome_negozio").value;
    email_user = document.getElementById("email_account_red").value;
    pw1 = document.getElementById("password_account_red1").value;
    pw2 = document.getElementById("password_account_red2").value;

    if (nome_negozio.trim() === "" || pw1.trim() === "" || pw2.trim() === ""){
      alert("Attenzione, riempire tutti i campi")
      return;
    }

    if(!isValidEmail(email_user)){
      alert("Attenzione, inserisci un'email valida.")
      return;
    }

    if(pw1 !== pw2){
      alert("Attenzione, le password devono coincidere.")
      return;
    }

    document.getElementById("form_migrate").submit();

  }



  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Passa dall'account blu a quello rosso</h3>
      <center>

        <form class="col s12" method="post" action="PHP_script/blue_to_red.php" id="form_migrate" enctype=”multipart/form-data”>

          <p class="center"><b>Inserisci il nome esatto del negozio (con account blu) già esistente.</b></p>
          <br>
          <div class="input-field col s12">
            <input id="nome_negozio" name="nome_negozio" type="text" class="validate">
            <label for="nome_negozio">Nome negozio</label>
          </div>

          <p class="center"><b>Inserisci le informazioni del tuo account.</b></p>
          <br>
          <div class="input-field col s12">
            <input id="email_account_red" name="email_account_red" type="email" class="validate">
            <label for="email_negozio" data-error="Inserisci un email valida">Email account</label>
          </div>
          <div class="input-field col s12">
            <input id="password_account_red1" name="password_account_red1" type="password" class="validate">
            <label for="password_account_red1" data-error="Inserisci una password valida">Password Account</label>
          </div>
          <div class="input-field col s12">
            <input id="password_account_red2" name="password_account_red2" type="password" class="validate">
            <label for="password_account_red2" data-error="Inserisci una password valida">Ripeti Password Account</label>
          </div>
          <p class="center">* L'iscrizione attualmente è completamente <b>gratuita</b> ma <b>potrebbe</b> variare dopo un periodo di prova di 3/6 mesi, per ammortizzare i <b>costi</b> sostenuti della piattaforma.
            La decisione avverrà sulla base dell'autofinanziamento possibile grazie alla pubblicità presente sull'applicazione mobile.</p>
          <button type="button" class="btn waves-effect blue" onclick="validateMigrateForm()">Invio
            <i class="material-icons right">send</i>
          </button>
        </form>
      </center>
    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
