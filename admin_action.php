<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="ADMINISTRATOR"){
    returnHome("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    function validateEvent(eventName){
      var r = confirm("Sei Sicuro di voler validare l'evento?");
      if (r == true) {
        location.href = "./PHP_script/move_event.php?nome_evento=" + eventName;
      }
    }

    function deleteEvent(eventName){
      var r = confirm("Sei Sicuro di voler cancellare l'evento?");
      if (r == true) {
        location.href = "./PHP_script/delete_event.php?nome_evento=" + eventName;
      }
    }

    function validateShop(shopName){
      var r = confirm("Sei Sicuro di voler validare il negozio?");
      if (r == true) {
        location.href = "./PHP_script/move_shop.php?nome_negozio=" + shopName;
      }
    }

    function deleteShop(shopName){
      var r = confirm("Sei Sicuro di voler cancellare il negozio?");
      if (r == true) {
        location.href = "./PHP_script/delete_shop.php?nome_negozio=" + shopName;
      }
    }

    function validateJob(jobName){
      var r = confirm("Sei Sicuro di voler validare il lavoro?");
      if (r == true) {
        location.href = "./PHP_script/move_job.php?nome_lavoro=" + jobName;
      }
    }

    function deleteJob(jobName){
      var r = confirm("Sei Sicuro di voler cancellare il lavoro?");
      if (r == true) {
        location.href = "./PHP_script/delete_job.php?nome_lavoro=" + jobName;
      }
    }

    function validateOffer(offerName){
      var r = confirm("Sei Sicuro di voler validare l'offerta?");
      if (r == true) {
        location.href = "./PHP_script/move_offer.php?nome_offerta=" + offerName;
      }
    }

    function deleteOffer(offerName){
      var r = confirm("Sei Sicuro di voler cancellare l'offerta?");
      if (r == true) {
        location.href = "./PHP_script/delete_offer.php?nome_offerta=" + offerName;
      }
    }

    function validateComproVendo(comprovendoName){
      var r = confirm("Sei Sicuro di voler validare il compro vendo?");
      if (r == true) {
        location.href = "./PHP_script/move_buysell.php?nome_comprovendo=" + comprovendoName;
      }
    }

    function deleteComproVendo(comprovendoName){
      var r = confirm("Sei Sicuro di voler cancellare il compro vendo?");
      if (r == true) {
        location.href = "./PHP_script/delete_buysell.php?nome_comprovendo=" + comprovendoName;
      }
    }

    function deleteAdvice(adviceName){
      var r = confirm("Sei Sicuro di voler cancellare il consiglio?");
      if (r == true) {
        location.href = "./PHP_script/delete_advice.php?nome_consiglio=" + adviceName;
      }
    }

    function deleteDelete(deleteName){
      var r = confirm("Sei Sicuro di voler cancellare la cancellazione?");
      if (r == true) {
        location.href = "./PHP_script/delete_delete.php?nome_cancellazione=" + deleteName;
      }
    }

    function setPharma(pharmaName){
      var r = confirm("Sei Sicuro di voler cambiare la Farmacia di Turno?");
      if (r == true) {
        location.href = "./PHP_script/set_pharma.php?nome_farmacia=" + pharmaName;
      }
    }

  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php

            if (isset($_COOKIE['messageAdminActionBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageAdminActionBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageAdminActionBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageAdminActionBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageAdminActionBarrafrancaConnect']);
              }
            }

            if ($_SESSION["role"]==="ADMINISTRATOR" && isset($_GET['action'])){
                echo "<div class='row'>";
                echo "<ul class='collection with-header'>";
                  echo "<li class='collection-header'><h4>Hai scelto: <b>".$_GET['action']."</b></h4></li>";
                echo "</ul>";
              }

              if(!isset($_GET['action'])){
                goToAdminHome("Devi scegliere qualcosa.");
                echo '<script>location.href = "admin_home.php";</script>';
                exit;
              }

              if($_GET['action'] === "eventi"){
                printCardRed("Da Validare");
                printNewEvents();
                printCardGreen("Esistenti");
                printEvents();
              } else if($_GET['action'] == "negozi"){
                printCardRed("Da Validare");
                printNewShops();
                printCardGreen("Esistenti");
                printShops();
              } else if($_GET['action'] == "lavori"){
                printCardRed("Da Validare");
                printNewJobs();
                printCardGreen("Esistenti");
                printJobs();
              } else if($_GET['action'] == "offerte"){
                printCardRed("Da Validare");
                printNewOffers();
                printCardGreen("Esistenti");
                printOffers();
              } else if($_GET['action'] == "comprovendo"){
                printCardRed("Da Validare");
                printNewComproVendo();
                printCardGreen("Esistenti");
                printComproVendo();
              } else if($_GET['action'] == "consigli"){
                printAdvice();
              } else if($_GET['action'] == "cancellazioni"){
                printDeleteRequest();
              } else if($_GET['action'] == "farmacie"){
                printPharma();
              } else {
                goToAdminHome("Errore. Non cambiare l'url a mano, scegli una delle azioni in basso.");
                echo '<script>location.href = "admin_home.php";</script>';
                exit;
              }
          ?>
          </div>
        </div>
      </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
