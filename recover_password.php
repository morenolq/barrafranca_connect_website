<?php
include ('./PHP_script/utility_php_bc.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    function validateRecoverPasswordForm1(){

      email_login = document.getElementById("login-email").value;
      login_type = document.getElementById("login-type").value;

      if (email_login.trim() === "" || login_type.trim() === ""){
            alert("Attenzione, inserisci sia email che password e scegli il tipo opportuno.");
            return;
          }

      if (!isValidEmail(email_login)){
        alert("L'email inserita non è valida.");
        return;
      }

      document.getElementById("recover-password-1-form").submit();

    }
  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Recupera Password</h3>

      <center>
            <div class="section">
              <?php
                if (isset($_COOKIE['messageLoginBarrafrancaConnect'])){
                    printCardRed($_COOKIE['messageLoginBarrafrancaConnect']);
                  }
              ?>

            </div>

            <div class="container">
              <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

                <form class="col s12" method="post" name="recover-password-1-form" id="recover-password-1-form" action="./PHP_script/update_password.php">

                  <div class='row'>
                    <div class="input-field col s12">
                      <select id="login-type" name="login-type">
                        <option value="" disabled>Seleziona Tipo Login</option>
                        <option value="ADMINISTRATOR">Ammistrazione</option>
                        <option value="USER_RED" selected>Utenti (Servizio Rosso)</option>
                        <option value="CREATOR" >Creatori (Foto)</option>
                        <option value="MUNICIPALITY" >Comune</option>
                      </select>
                      <label>Tipo di Login</label>
                    </div>
                  </div>

                  <div class='row'>
                    <div class='input-field col s12'>
                      <input class='validate' type='email' name='login-email' id='login-email' />
                      <label for='email'>Inserisci email</label>
                    </div>
                    <label style='float: right;'>
      								<p class='red-text'>L'email è necessaria.</p>
      							</label>
                  </div>


                  <br/>
                  <center>
                    <div class='row'>
                      <button type='button' onclick="validateRecoverPasswordForm1()" name='btn_login' class='col s12 btn btn-large waves-effect blue'>Avanti</button>
                    </div>
                  </center>
                </form>
              </div>
            </div>
          </center>
    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
