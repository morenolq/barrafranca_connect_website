<?php
session_start();

include("./PHP_script/utility_php_bc.php");

if (!isset($_SESSION["username"])) {
  returnHome("Devi aver effettuato il login per cambiare la password");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

      function validateChangePassword(){

        current_password = document.getElementById("current_password").value;
        new_password_1 = document.getElementById("new_password_1").value;
        new_password_2 = document.getElementById("new_password_2").value;

        if (current_password.trim() === "" || new_password_1.trim() === "" || new_password_2.trim() === ""){
              alert("Attenzione, inserisci tutti i campi");
              return;
            }

        if (new_password_1.length < 8 || new_password_2.length < 8){
          alert("Inserisci una nuova password di almeno 8 caratteri");
          return;
        }

        if (new_password_1 !== new_password_2){
          alert("Le Password non coincidono, riprova.");
          return;
        }

        document.getElementById("changePasswordForm").submit();

      }
  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>
      <h3 class="header center orange-text">Cambia Password</h3>
      <center>

            <div class="container">
              <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 40px 32px 40px; border: 1px solid #EEE;">

                <form class="col s12" method="post" name="changePasswordForm" id="changePasswordForm" action="PHP_script/change_password_script.php">

                  <div class='row'>
                    <div class='input-field col s12' style="width: 250px">
                      <input type='password' name='current_password' id='current_password' onkeypress="if (event.keyCode==13){ validateChangePassword(); }" />
                      <label for='email'>Password attuale</label>
                    </div>
                  </div>

                  <div class='row'>
                    <div class='input-field col s12' style="width: 250px">
                      <input type='password' name='new_password_1' id='new_password_1' onkeypress="if (event.keyCode==13){ validateChangePassword(); }" />
                      <label for='email'>Nuova Password</label>
                    </div>
                  </div>

                  <div class='row'>
                    <div class='input-field col s12' style="width: 250px">
                      <input type='password' name='new_password_2' id='new_password_2' onkeypress="if (event.keyCode==13){ validateChangePassword(); }" />
                      <label for='password'>Reinserisci nuova password</label>
                    </div>
                  </div>

                  <br/>
                  <center>
                    <div class='row'>
                      <button type='button' onclick="validateChangePassword()" name='btn_login' class='col s12 btn btn-large waves-effect blue'>Cambia Password</button>
                    </div>
                  </center>
                </form>
              </div>
            </div>
          </center>

    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
