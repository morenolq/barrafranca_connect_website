<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="CREATOR"){
    returnHome("Errore, pannello riservato ai creatori (per caricare foto).");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/materialize.clockpicker.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <link href="css/loading.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="/js/pdfobject.js"></script>
  <script src="./js/pdfobject.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/materialize.clockpicker.js"></script>
  <script src="js/init.js"></script>


  <script>

  </script>

  <style>
	.pdfobject-container { height: 500px;}
	.pdfobject { border: 1px solid #666; }
  </style>

</head>
<body>
  <div class="loading" id="loading" style="display:none">Loading&#8230;</div>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php //MESSAGE + WELCOME
            if (isset($_COOKIE['messageCreatorHomeBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageCreatorHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageCreatorHomeBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageCreatorHomeBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageCreatorHomeBarrafrancaConnect']);
              }
            }

            if (isset($_SESSION['nome_creatore'])){
                printCardBlue("Benvenuto <b>" . $_SESSION['nome_creatore'] . "</b>");
              }

          ?>

          <center>
            <form class="col s12" method="post" action="PHP_script/upload_photo_album.php" id="form-album-creator" enctype="multipart/form-data">

              <div class="row">
                <div class="input-field col s12">
                  <input id="nome_album" name="nome_album" type="text" class="validate">
                  <label for="nome_album">Nome Album</label>
                </div>
              </div>

              <div class="input-field col s12">
  		          <i class="material-icons prefix">mode_edit</i>
                  <textarea id="descrizione_album" name="descrizione_album" class="materialize-textarea"></textarea>
                  <label for="descrizione_album">Descrizione</label>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <input id="link_creatore" name="link_creatore" type="text" class="validate">
                  <label for="link_creatore">Link Creatore (Social, sito Web...)</label>
                </div>
              </div>

              <div class="row">
                <div class='file-field input-field'>
                  <div class='btn waves-effect red'>
                    <span>File</span>
                    <input id='images_album' name='files[]' type='file' accept="image/*" multiple data-maxfilesize="1000000">
                  </div>
                  <div class='file-path-wrapper'>
                    <input disabled class='file-path validate' type='text' placeholder='Carica le foto'>
                  </div>
                </div>
              </div>

              <div class="row">
                <p class="red-text text" align="center">1. Per una visualizzazione ottimale da smartphone, sono accettate solamente delle foto di dimensione massima 1.5 MB.</p>
                <p class="red-text text" align="center">2. Per il momento, una volta caricate le foto, esse non saranno più modificabili.</p>
              </div>

              <br>


              <button type="button" class="btn waves-effect blue" onclick="validateAlbumForm()">Invio
                <i class="material-icons right">send</i>
              </button>
            </form>

            <br>
          </center>

        </div>

    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>

  <script>

    function validateAlbumForm(){
      nome_album = document.getElementById("nome_album").value;
      descrizione_album = document.getElementById("descrizione_album").value;
      link_creatore = document.getElementById("link_creatore").value;
      images_album = document.getElementById("images_album");

      if (nome_album.trim() === ""){
        alert("Inserire un nome per l'album.");
        return;
      }

      if (descrizione_album.trim() === ""){
        alert("Inserire una descrizione per l'album.");
        return;
      }

      if (link_creatore.trim() === ""){
        alert("Inserire un link ai social o sito web del creatore");
        return;
      }

      if (!images_album.files[0]){
        alert("Attenzione, devi caricare almeno una foto, queste non saranno più modificabili una volta caricate.");
        return;
      }
      document.getElementById("loading").style.display = "block";
      document.getElementById("form-album-creator").submit();

    }



  </script>

  </body>
</html>
