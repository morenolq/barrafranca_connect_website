<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if (isset($_SESSION["role"])){
    returnHome("Esci dal tuo account prima di registrarti.");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="js/jquery-3.2.1.min.js"></script>
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    function validateShopForm(){
      nome_negozio = document.getElementById("nome_negozio").value;
      indirizzo_negozio = document.getElementById("indirizzo_negozio").value;
      email_negozio = document.getElementById("email_negozio").value;
      telefono_negozio = document.getElementById("telefono_negozio").value;
      descrizione_negozio = document.getElementById("descrizione_negozio").value;
      categoria_negozio = document.getElementById("categoria_negozio").value;
      come_contattarti = document.getElementById("come_contattarti").value;
      email_account_red = document.getElementById("email_account_red").value;
      password_account_red1 = document.getElementById("password_account_red1").value;
      password_account_red2 = document.getElementById("password_account_red2").value;
      cbInfo = document.getElementById("cbInfo").checked;


      if (nome_negozio.trim() === "" || indirizzo_negozio.trim() === "" || telefono_negozio.trim() === "" ||
          email_negozio.trim() === "" || descrizione_negozio.trim() === "" || categoria_negozio.trim() === "" ||
          come_contattarti.trim() === "" || email_account_red.trim() === "" || password_account_red1.trim() === "" ||
          password_account_red2.trim() === ""){
            alert("Attenzione, devono essere riempiti tutti i campi.");
            return;
          }

      if (!isValidEmail(email_negozio)){
        alert("L'email del negozio inserita non è valida.");
        return;
      }

      if (!isValidEmail(email_account_red)){
        alert("L'email utente inserita non è valida.");
        return;
      }

      if (!isValidPhoneNumber(telefono_negozio)){
        alert("Il numero di telefono inserito non è valido.");
        return;
      }

      if(password_account_red1 !== password_account_red2){
        alert("Le password non coincidono.");
        return;
      }

      if (!cbInfo){
        alert("Devi accettare la normativa sulla privacy");
        return;
      }

      document.getElementById("red-signin-form").submit();

    }
  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Crea subito il tuo account</h3>
      <p class="center"><b>Inserisci le informazioni più chiare e corrette possibili, saranno la tua vetrina sull'applicazione.</b></p>
      <center>
        <form class="col s12" method="post" action="PHP_script/insert_user_red.php" id="red-signin-form" enctype=”multipart/form-data”>
          <div class="row">
            <div class="input-field col s12">
              <input id="nome_negozio" name="nome_negozio" type="text" class="validate">
              <label for="nome_negozio">Nome Attività</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="indirizzo_negozio" name="indirizzo_negozio" type="text" class="validate">
              <label for="indirizzo_negozio">Indirizzo Attività</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <input id="email_negozio" name="email_negozio" type="email" class="validate">
              <label for="email_negozio" data-error="Inserisci un email valida">Email</label>
            </div>
            <div class="input-field col s6">
              <input id="telefono_negozio" name="telefono_negozio" type="tel" class="validate">
              <label for="telefono_negozio" data-error="Inserisci un numero valido">Numero Telefonico</label>
            </div>
          </div>
          <div class="input-field col s12">
		      <i class="material-icons prefix">mode_edit</i>
            <textarea id="descrizione_negozio" name="descrizione_negozio" class="materialize-textarea"></textarea>
            <label for="address">Descrizione</label>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <select id="giorno_chiusura_negozio" name="giorno_chiusura_negozio">
                <option value="" disabled selected>Giorno di Chiusura</option>
                <option value="Lunedì">Nessuno</option>
                <option value="Lunedì">Lunedì</option>
                <option value="Martedì">Martedì</option>
                <option value="Mercoledì">Mercoledì</option>
                <option value="Giovedì">Giovedì</option>
                <option value="Venerdì">Venerdì</option>
                <option value="Sabato">Sabato</option>
                <option value="Domenica">Domenica</option>
              </select>
              <label>Giorno di Chiusura (non obbligatorio)</label>
            </div>
            <div class="input-field col s6">
              <input id="orario_negozio" name="orario_negozio" type="text" class="validate">
              <label for="orario_negozio">Orari d'apertura (9 - 13 / 16 - 20)</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <select id="categoria_negozio" name="categoria_negozio">
                <option value="" disabled selected>Seleziona Categoria</option>
                <option class="black-text" value="FOOD_DRINK">Food &amp; Drink</option>
                <option value="RISTORANTI_PIZZERIE">Ristoranti e Pizzerie</option>
                <option value="BAR_TABACCHI">Bar e Tabacchi</option>
                <option value="ABBIGLIAMENTO_CORREDO">Abbigliamento e Corredo</option>
                <option value="EDICOLE_CARTOLERIE">Edicole e Tipografie</option>
                <option value="OGGETTISTICA_PELLETTERIA">Oggettistica e Pelletteria</option>
                <option value="ESTETICA_SALUTE">Estetica e Salute</option>
                <option value="SUPERMERCATI_ALIMENTARI">Supermercati e Alimentari</option>
                <option value="AGRICOLTURA_PRODOTTIAGRICOLI">Agricoltura/Prodotti Agricoli</option>
                <option value="CASA_EDILIZIA">Casa, Edilizia e Ferramenta</option>
                <option value="AUTO_MOTO">Auto e Moto</option>
                <option value="FESTE_EVENTI">Feste ed Eventi</option>
                <option value="LIBERI_PROFESSIONISTI">Liberi Professionisti</option>
                <option value="ELETTRONICA_TELEFONIA">Elettrodomestici e Telefonia</option>
                <option value="SERVIZI">Servizi</option>
                <option value="ALTRO">Altro</option>
              </select>
              <label>Seleziona una Categoria</label>
            </div>
            <div class="input-field col s6">
              <input id="come_contattarti" name="come_contattarti" type="tel" class="validate">
              <label for="come_contattarti" data-error="Inserisci un numero valido">Recapito per contattarti</label>
            </div>
          </div>
          <br>
          <p class="center"><b>Inserisci ora le informazioni del tuo account.</b></p>
          <br>

          <div class="input-field col s12">
            <input id="email_account_red" name="email_account_red" type="email" class="validate">
            <label for="email_negozio" data-error="Inserisci un email valida">Email account</label>
          </div>

          <div class="input-field col s12">
            <input id="password_account_red1" name="password_account_red1" type="password" class="validate">
            <label for="email_negozio" data-error="Inserisci un email valida">Password Account</label>
          </div>

          <div class="input-field col s12">
            <input id="password_account_red2" name="password_account_red2" type="password" class="validate">
            <label for="email_negozio" data-error="Inserisci un email valida">Ripeti Password Account</label>
          </div>

          <p class="input-field col s12">
            <input type="checkbox" id="cbInfo"/>
            <label for="cbInfo">Accetto l'<a href="./info_privacy.php">informativa sulla privacy</a></label>
          </p>

          <br>

          <button type="button" class="btn waves-effect blue" onclick="validateShopForm()">Invio
            <i class="material-icons right">send</i>
          </button>
        </form>
      </center>

    </div>
  </div>

  <br><br>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
          <!--<p class="grey-text text-lighten-4">Usando il sito si accetta l'<a style="color:red;" href="./info_privacy.php">informativa sulla privacy</p>-->
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
