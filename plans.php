<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username']) && isset($_SESSION['role'])){

          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";

          if ($_SESSION['role'] === "ADMINISTRATOR"){
            echo "<ul class='right hide-on-med-and-down'>";
              echo "<li><a href='./admin_home.php'>Home Admin</a></li>";
            echo "</ul>";
          }

        } else {
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='login.php'>Entra nell'area personale</a></li>";
          echo "</ul>";
        }

      ?>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <!--<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>-->
    </nav>
    </div>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h2 class="header center orange-text">Ecco tutte le opportunità</h2>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">

        <div class="col s12 m12" align="center"><!--   m6 for double coloumns   -->
          <div class="icon-block">
            <!-- <h2 class="center red-text"><i class="fa fa-car" aria-hidden="true"></i></h2>-->
            <h5 class="center">Unico Profilo (Rosso)</h5>

            <br>

            <table class = "bordered striped centered">
              <tbody>
                <tr>
                  <td>Prezzo</td>
                  <td><b>Gratuito*</b></td>
                </tr>
                <tr>
                  <td>Attività</td>
                  <td><h5 class="center green-text"><i class="material-icons">thumb_up</i></h5></td>
                </tr>
                <tr>
                  <td>Offerte</td>
                  <td><h5 class="center green-text"><i class="material-icons">thumb_up</i></h5></td>
                </tr>
                <tr>
                  <td>Eventi</td>
                  <td><h5 class="center green-text"><i class="material-icons">thumb_up</i></h5></td>
                </tr>
              </tbody>
            </table>

            <br>

            <a href="signin_red.php" class="waves-effect waves-light btn red">Iscriviti Gratis</a>

            <p class="light" align="center">
              Ideato per tutti coloro i quali vogliono inserire nella piattaforma degli <b>eventi</b> o delle <b>offerte</b>. Permette di creare un account e gestire facilmente offerte ed eventi.
              <p>* La creazione dell'account e l'inserimento di informazioni è completamente <b>GRATUITO</b>, in futuro potrebbe essere valutato un pagamento per l'inserzione di Volantini e Eventi. <b>Non faremo nulla di tutto ciò se l'applicazione potrà autosostenersi!</b><p>
            </p>


          </div>
        </div>
      </div>

    </div>

  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
