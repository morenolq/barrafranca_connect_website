<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="USER_RED"){
    returnHome("Errore, pannello riservato agli utenti del servizio Rosso.");
    exit;
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="/js/pdfobject.js"></script>
  <script src="./js/pdfobject.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>

  <script>
  function validateShopUpdate(){
    nome_negozio = document.getElementById("nome_negozio").value;
    indirizzo_negozio = document.getElementById("indirizzo_negozio").value;
    email_negozio = document.getElementById("email_negozio").value;
    telefono_negozio = document.getElementById("telefono_negozio").value;
    descrizione_negozio = document.getElementById("descrizione_negozio").value;
    categoria_negozio = document.getElementById("categoria_negozio").value;
    come_contattarti = document.getElementById("come_contattarti").value;


    if (nome_negozio.trim() === "" || indirizzo_negozio.trim() === "" || telefono_negozio.trim() === "" ||
        email_negozio.trim() === "" || descrizione_negozio.trim() === "" || categoria_negozio.trim() === "" ||
        come_contattarti.trim() === ""){
          alert("Attenzione, devono essere riempiti tutti i campi.");
          return;
        }

    if (!isValidEmail(email_negozio)){
      alert("L'email del negozio inserita non è valida.");
      return;
    }

    if (!isValidPhoneNumber(telefono_negozio)){
      alert("Il numero di telefono inserito non è valido.");
      return;
    }

    document.getElementById("red-change-info-form").submit();

  }

    $(document).ready(function() {
        $('select').material_select();
      });
  </script>

</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php //MESSAGE + WELCOME
            if (isset($_COOKIE['messageUserRedHomeBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageUserRedHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageUserRedHomeBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageUserRedHomeBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageUserRedHomeBarrafrancaConnect']);
              }
            }

            if (isset($_SESSION['username'])){
                printCardBlue("Benvenuto <b>" . $_SESSION['nome_negozio'] . "</b>");
              }


          if (!isset($_SESSION["nome_negozio"])){
            returnHome("Errore, pannello riservato, fai l'accesso e riprova.");
            exit;
          }

          //for each in table print in editable. ATTENTION UPDATE SESSION VARIABLE
          $conn = getConnection();

          // Check connection
          if (!$conn) {
              goToUserRedHome("Errore di connessione con il database, riprova.");
          }

          mysqli_query($conn, "SET NAMES 'utf8'");
          mysql_set_charset("utf8");

          $sql = "SELECT *
                  FROM  negozi_barrafranca
                  WHERE nome_negozio = '".$_SESSION["nome_negozio"]."'";

          $result = mysqli_query($conn, $sql);

          $r = mysqli_fetch_assoc($result);

          echo "<center>";
            echo "<form class='col s12' method='post' action='PHP_script/change_info_red_script.php' id='red-change-info-form' enctype=”multipart/form-data”>";
              echo "<div class='row'>";
                echo "<div class='input-field col s12'>";
                  echo "<input disabled value='".$r["nome_negozio"]."' id='nome_negozio' name='nome_negozio' type='text' class='validate'>";
                  echo "<label for='nome_negozio'>Nome Attività</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s12'>";
                  echo "<input value='".$r["indirizzo_negozio"]."' id='indirizzo_negozio' name='indirizzo_negozio' type='text' class='validate'>";
                  echo "<label for='indirizzo_negozio'>Indirizzo Attività</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s6'>";
                  echo "<input value='".$r["email_negozio"]."' id='email_negozio' name='email_negozio' type='email' class='validate'>";
                  echo "<label for='email_negozio' data-error='Inserisci un email valida'>Email</label>";
                echo "</div>";
                echo "<div class='input-field col s6'>";
                  echo "<input value='".$r["telefono_negozio"]."' id='telefono_negozio' name='telefono_negozio' type='tel' class='validate'>";
                  echo "<label for='telefono_negozio' data-error='Inserisci un numero valido'>Numero Telefonico</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='input-field col s12'>";
    		      echo "<i class='material-icons prefix'>mode_edit</i>";
                echo "<textarea id='descrizione_negozio' name='descrizione_negozio' class='materialize-textarea'>".$r["descrizione_negozio"]."</textarea>";
                echo "<label for='address'>Descrizione</label>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s6'>";
                  echo "<select id='giorno_chiusura_negozio' name='giorno_chiusura_negozio'>";
                    echo "<option value='' disabled>Giorno di Chiusura</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Nessuno") { echo "selected"; } echo " value='Nessuno'>Nessuno</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Lunedì") { echo "selected"; } echo " value='Lunedì'>Lunedì</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Martedì") { echo "selected"; } echo " value='Martedì'>Martedì</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Mercoledì") { echo "selected"; } echo " value='Mercoledì'>Mercoledì</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Giovedì") { echo "selected"; } echo " value='Giovedì'>Giovedì</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Venerdì") { echo "selected"; } echo " value='Venerdì'>Venerdì</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Sabato") { echo "selected"; } echo " value='Sabato'>Sabato</option>";
                    echo "<option "; if ($r["giorno_chiusura_negozio"]=="Domenica") { echo "selected"; } echo " value='Domenica'>Domenica</option>";
                  echo "</select>";
                  echo "<label>Giorno di Chiusura (non obbligatorio)</label>";
                echo "</div>";
                echo "<div class='input-field col s6'>";
                  echo "<input value='".$r["orario_negozio"]."' id='orario_negozio' name='orario_negozio' type='text' class='validate'>";
                  echo "<label for='orario_negozio'>Orario (9 - 13 / 16 - 20)</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s6'>";
                  echo "<select id='categoria_negozio' name='categoria_negozio'>";
                    echo "<option value='' disabled>Seleziona Categoria</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="FOOD_DRINK") { echo "selected"; } echo " class='black-text' value='FOOD_DRINK'>Food &amp; Drink</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="ABBIGLIAMENTO_CORREDO") { echo "selected"; } echo " value='ABBIGLIAMENTO_CORREDO'>Abbigliamento e Corredo</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="EDICOLE_CARTOLERIE") { echo "selected"; } echo " value='EDICOLE_CARTOLERIE'>Edicole e Cartolerie</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="OGGETTISTICA_PELLETTERIA") { echo "selected"; } echo " value='OGGETTISTICA_PELLETTERIA'>Oggettistica e Pelletteria</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="ESTETICA_SALUTE") { echo "selected"; } echo " value='ESTETICA_SALUTE'>Estetica e Salute</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="SUPERMERCATI_ALIMENTARI") { echo "selected"; } echo " value='SUPERMERCATI_ALIMENTARI'>Supermercati e Alimentari</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="AUTO_MOTO") { echo "selected"; } echo " value='AUTO_MOTO'>Auto e Moto</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="SERVIZI") { echo "selected"; } echo " value='SERVIZI'>Servizi</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="LIBERI_PROFESSIONISTI") { echo "selected"; } echo " value='LIBERI_PROFESSIONISTI'>Liberi Professionisti</option>";
                    echo "<option "; if ($r["categoria_negozio"]=="ELETTRONICA_TELEFONIA") { echo "selected"; } echo " value='ELETTRONICA_TELEFONIA'>Elettronica e Telefonia</option>";
                  echo "</select>";
                  echo "<label>Seleziona una Categoria</label>";
                echo "</div>";
                echo "<div class='input-field col s6'>";
                  echo "<input value='".$r["come_contattarti"]."'id='come_contattarti' name='come_contattarti' type='tel' class='validate'>";
                  echo "<label for='come_contattarti' data-error='Inserisci un numero valido'>Recapito per contattarti</label>";
                echo "</div>";
              echo "</div>";

              echo "<button type='button' class='btn waves-effect blue' onclick='validateShopUpdate()'>Cambia Informazioni";
                echo "<i class='material-icons right'>done</i>";
              echo "</button>";
              echo "<p> </p>";
              echo "<button type='button' class='btn waves-effect red' onclick='location.href=\"userred_home.php\"'>Annulla";
                echo "<i class='material-icons right'>cancel</i>";
              echo "</button>";
            echo "</form>";
          echo "</center>";

        ?>

         <br>

    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
