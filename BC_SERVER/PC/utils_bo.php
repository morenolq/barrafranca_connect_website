<?php

function showNewBuysells(){

  include("connect_db.php");
  // Create connection
  $conn = getConnection();

  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  nuovi_comprovendo_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              echo "<table id='table' class='w3-table w3-bordered w3-striped'>";
              echo "<tbody>";
              while($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>";
                  echo "<td><button type='button' onclick='validateRow(\"".$row["nome_comprovendo"]."\")' >APPROVA</button></td>";
                  echo "<td>";
                  echo "#TITOLOANNUNCIO","<br>";
                  echo $row["nome_comprovendo"], "<br>";
                  echo "#PREZZOANNUNCIO", "<br>";
                  echo $row["prezzo_comprovendo"], "<br>";
                  echo "#NOMECONTATTOANNUNCIO", "<br>";
                  echo $row["nome_contatto_comprovendo"], "<br>";

                  if(!empty($row["email_comprovendo"])){
                          echo "#EMAILCONTATTOANNUNCIO", "<br>";
                          echo $row["email_comprovendo"], "<br>";
                  }


                  if(!empty($row["telefono_comprovendo"])){
                          echo "#TELEFONOCONTATTOANNUNCIO", "<br>";
                          echo $row["telefono_comprovendo"], "<br>";
                  }

                  echo "#DESCRIZIONEANNUNCIO", "<br>";
                  echo $row["descrizione_comprovendo"], "<br>";
                  echo "#COMECONTATTAREANNUNCIO", "<br>";
                  echo $row["come_contattarti"], "<br>";
                  echo "#FINEANNUNCIO", "<br>";
                  echo "</td>";
                  echo "</tr>";
              }
            echo "<br>", "<br>";
            echo "</tbody>";
            echo "</table>";
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);


}

function showNewEvents(){

  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  nuovi_eventi_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              echo "<table id='table' class='w3-table w3-bordered w3-striped'>";
              while($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>";
                  echo "<td><button type='button' onclick='validateRow(\"".$row["nome_evento"]."\")' >APPROVA</button></td>";
                  echo "<td>";
                  echo "#NOMEEVENTO","<br>";
                  echo $row["nome_evento"], "<br>";
                  echo "#DATAEVENTO", "<br>";
                  echo $row["data_evento"], "<br>";
                  echo "#ORARIOEVENTO", "<br>";
                  echo $row["ora_evento"], "<br>";

                  if(!empty($row["email_evento"])){
                          echo "#EMAILCONTATTOEVENTO", "<br>";
                          echo $row["email_evento"], "<br>";
                  }


                  if(!empty($row["telefono_evento"])){
                          echo "#TELEFONOCONTATTOEVENTO", "<br>";
                          echo $row["telefono_evento"], "<br>";
                  }

                  echo "#ORGANIZZATOREEVENTO", "<br>";
                  echo $row["nome_organizzatore_evento"], "<br>";
                  echo "#DESCRIZIONEEVENTO", "<br>";
                  echo $row["descrizione_evento"], "<br>";
                  echo "#COMECONTATTAREEVENTO", "<br>";
                  echo $row["come_contattarti"], "<br>";
                  echo "#FINEEVENTO", "<br>";
                  echo "</td>";
                  echo "</tr>";
              }
              echo "<br>", "<br>";
              echo "</table>";
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);
}

function showNewOffers(){
  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  nuove_offerte_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              echo "<table id='table' class='w3-table w3-bordered w3-striped'>";
              while($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>";
                  echo "<td><button type='button' onclick='validateRow(\"".$row["nome_offerta"]."\")' >APPROVA</button></td>";
                  echo "<td>";
                  echo "#NEGOZIOOFFERTA","<br>";
                  echo $row["nome_offerta"], "<br>";
                  echo "#VALIDITAOFFERTA", "<br>";
                  echo $row["data_inizio_offerta"] . " - " . $row["data_fine_offerta"], "<br>";

                  if(!empty($row["link_pdf"])){
                          echo "#PDFURLOFFERTA", "<br>";
                          echo $row["link_pdf"], "<br>";
                  }

                  if(!empty($row["links_img"])){
                          echo "#STARTIMAGESURLOFFERTA", "<br>";
                          echo $row["links_img"], "<br>";
                          echo "#ENDIMAGESURLOFFERTA", "<br>";
                  }

                  echo "#FINEOFFERTA";
                  echo "</td>";
                  echo "</tr>";
              }
              echo "<br>", "<br>";
              echo "</table>";
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);
}

function showNewShops(){
  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  nuovi_negozi_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              echo "<table id='table' class='w3-table w3-bordered w3-striped'>";
              while($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>";
                  echo "<td><button type='button' onclick='validateRow(\"".$row["nome_negozio"]."\")' >APPROVA</button></td>";
                  echo "<td>";

                  echo "#NOMENEGOZIO","<br>";
                  echo $row["nome_negozio"], "<br>";
                  echo "#INDIRIZZONEGOZIO", "<br>";
                  echo $row["indirizzo_negozio"], "<br>";
                  echo "#DESCRIZIONENEGOZIO", "<br>";
                  echo $row["descrizione_negozio"], "<br>";

                  if(!empty($row["telefono_negozio"])){
                          echo "#TELEFONONEGOZIO", "<br>";
                          echo $row["telefono_negozio"], "<br>";
                  }

                  if(!empty($row["email_negozio"])){
                          echo "#EMAILNEGOZIO", "<br>";
                          echo $row["email_negozio"], "<br>";
                  }

                  echo "#CATEGORIANEGOZIO", "<br>";
                  echo $row["categoria_negozio"], "<br>";
                  echo "#COMECONTATTARENEGOZIO", "<br>";
                  echo $row["come_contattarti"], "<br>";
                  echo "#FINENEGOZIO";
                  echo "</td>";
                  echo "</tr>";
              }
              echo "<br>", "<br>";
              echo "</table>";
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);
}

function showNewJobs(){

  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  nuovi_lavori_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              echo "<table id='table' class='w3-table w3-bordered w3-striped'>";
              while($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>";
                  echo "<td><button type='button' onclick='validateRow(\"".$row["nome_lavoro"]."\")' >APPROVA</button></td>";

                  echo "<td>";
                  echo "#MANSIONELAVORO","<br>";
                  echo $row["nome_lavoro"], "<br>";
                  echo "#NOMECONTATTO", "<br>";
                  echo $row["nome_contatto_lavoro"], "<br>";

                  if(!empty($row["email_lavoro"])){
                          echo "#EMAILCONTATTO", "<br>";
                          echo $row["email_lavoro"], "<br>";
                  }

                  if(!empty($row["telefono_lavoro"])){
                          echo "#TELEFONOCONTATTO", "<br>";
                          echo $row["telefono_lavoro"], "<br>";
                  }

                  echo "#INDIRIZZOLAVORO", "<br>";
                  echo $row["indirizzo_lavoro"], "<br>";
                  echo "#DESCRIZIONELAVORO", "<br>";
                  echo $row["descrizione_lavoro"], "<br>";
                  echo "#COMECONTATTARELAVORO", "<br>";
                  echo $row["come_contattarti"], "<br>";
                  echo "#FINELAVORO";
                  echo "</td>";
                  echo "</tr>";
              }
              echo "<br>", "<br>";
              echo "</table>";
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);

}

function showNewAdvices() {

  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  avvisi_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              while($row = mysqli_fetch_assoc($result)) {

                  echo "#--NOME--CONSIGLIO","<br>";
                  echo $row["nome_consiglio"], "<br>";
                  echo "#--DESCRIZIONE--CONSIGLIO", "<br>";
                  echo $row["descrizione_consiglio"], "<br>";
                  echo "<br>", "<br>";

              }
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);

}

function showNewDelete(){
  include("connect_db.php");
  // Create connection
  $conn = getConnection();
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      echo "ERRORE connessione database";
  }

  mysqli_query($conn, "SET NAMES 'utf8'");

  $sql = "SELECT * FROM  cancellazione_barrafranca";

  $result = mysqli_query($conn, $sql);

  if (!$result) {
      echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
  } else {
      if (mysqli_num_rows($result) > 0) {
              // output data of each row
              while($row = mysqli_fetch_assoc($result)) {

                  echo "#--NOME--CANCELLAZIONE","<br>";
                  echo $row["nome_cancellazione"], "<br>";
                  echo "#--MOTIVO--CANCELLAZIONE", "<br>";
                  echo $row["motivo_cancellazione"], "<br>";
                  echo "#--CATEGORIA--CANCELLAZIONE", "<br>";
                  echo $row["categoria_cancellazione"], "<br>";
                  echo "#--COME--CONTATTARE", "<br>";
                  echo $row["come_contattarti"], "<br>";
                  echo "<br>", "<br>";

              }
          } else {
              echo "0 results";
          }

  }

  mysqli_close($conn);
}

?>
