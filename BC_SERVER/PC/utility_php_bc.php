<?php

function mysqlCleaner($conn, $data)
{
	$data = mysqli_real_escape_string($conn, $data);
	$data = stripslashes($data);
	$data = htmlentities($data);
	return $data;
	//or in one line code
	//return(stripslashes(mysql_real_escape_string($data)));
}

function encryptPassword($password, $sale){

	$saltedPW =  $password . $sale;
	$hashedPW = hash('sha256', $saltedPW);
	return $hashedPW;

}

function destroysession(){
	$_SESSION = array();
	if (ini_get("session.use_cookies")){
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time()- 3600*24, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	}
	session_destroy();
}

function return_home($message){

	//if ($message == "Login Scaduto"){
	//	destroysession();
	//}

	setcookie("messagehome", $message, time() + 5, "/", "localhost");
	header("location: Barrafranca_Connect/index.php");

}

 ?>
