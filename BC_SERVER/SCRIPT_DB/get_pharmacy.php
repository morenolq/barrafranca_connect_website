<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  farmacie_barrafranca ORDER BY diturno_farmacia DESC";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

              echo "#NOMEFARMACIA","\n";
              echo $row["nome_farmacia"], "\n";
              echo "#INDIRIZZOFARMACIA","\n";
              echo $row["indirizzo_farmacia"], "\n";
              echo "#TELEFONOFARMACIA", "\n";
              echo $row["telefono_farmacia"], "\n";
              echo "#DITURNOFARMACIA", "\n";
              echo $row["diturno_farmacia"], "\n";
              echo "#FINEFARMACIA";
              echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
