<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_comprovendo = mysqli_real_escape_string($conn, $_GET["nome_comprovendo"]);
$prezzo_comprovendo = mysqli_real_escape_string($conn, $_GET["prezzo_comprovendo"]);
$nome_contatto_comprovendo = mysqli_real_escape_string($conn, $_GET["nome_contatto_comprovendo"]);
$email_comprovendo = mysqli_real_escape_string($conn, $_GET["email_comprovendo"]);
$telefono_comprovendo = mysqli_real_escape_string($conn, $_GET["telefono_comprovendo"]);
$descrizione_comprovendo = mysqli_real_escape_string($conn, $_GET["descrizione_comprovendo"]);
$come_contattarti = mysqli_real_escape_string($conn, $_GET["come_contattarti"]);

$sql = "INSERT INTO nuovi_comprovendo_barrafranca (nome_comprovendo, prezzo_comprovendo, nome_contatto_comprovendo, email_comprovendo, telefono_comprovendo, descrizione_comprovendo, come_contattarti)
VALUES ('".$nome_comprovendo."', '".$prezzo_comprovendo."', '".$nome_contatto_comprovendo."', '".$email_comprovendo."', '".$telefono_comprovendo."', '".$descrizione_comprovendo."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato un COMPRO_VENDO<br><br>
             ".$nome_comprovendo."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO COMPRO_VENDO Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
