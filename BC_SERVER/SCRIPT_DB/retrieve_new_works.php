<?php

if (!isset($_COOKIE["cookie_connection_barrafrancaonline"])){
    header("location: index.php");
}

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  nuovi_lavori_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

                echo "#MANSIONELAVORO","<br>";
                echo $row["nome_lavoro"], "<br>";
                echo "#NOMECONTATTO", "<br>";
                echo $row["nome_contatto_lavoro"], "<br>";

                if(!empty($row["email_lavoro"])){
                        echo "#EMAILCONTATTO", "<br>";
                        echo $row["email_lavoro"], "<br>";
                }

                if(!empty($row["telefono_lavoro"])){
                        echo "#TELEFONOCONTATTO", "<br>";
                        echo $row["telefono_lavoro"], "<br>";
                }

                echo "#INDIRIZZOLAVORO", "<br>";
                echo $row["indirizzo_lavoro"], "<br>";
                echo "#DESCRIZIONELAVORO", "<br>";
                echo $row["descrizione_lavoro"], "<br>";
                echo "#COMECONTATTARELAVORO", "<br>";
                echo $row["come_contattarti"], "<br>";
                echo "#FINELAVORO";
                echo "<br>", "<br>";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
