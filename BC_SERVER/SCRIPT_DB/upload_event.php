<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_evento = mysqli_real_escape_string($conn, $_GET["nome_evento"]);
$data_evento = mysqli_real_escape_string($conn, $_GET["data_evento"]);
$ora_evento = mysqli_real_escape_string($conn, $_GET["ora_evento"]);
$email_evento = mysqli_real_escape_string($conn, $_GET["email_evento"]);
$telefono_evento = mysqli_real_escape_string($conn, $_GET["telefono_evento"]);
$nome_organizzatore_evento = mysqli_real_escape_string($conn, $_GET["nome_organizzatore_evento"]);
$descrizione_evento = mysqli_real_escape_string($conn, $_GET["descrizione_evento"]);
$come_contattarti = mysqli_real_escape_string($conn, $_GET["come_contattarti"]);

$sql = "INSERT INTO nuovi_eventi_barrafranca (nome_evento, data_evento, ora_evento, email_evento, telefono_evento, nome_organizzatore_evento, descrizione_evento, come_contattarti)
VALUES ('".$nome_evento."', '".$data_evento."', '".$ora_evento."', '".$email_evento."', '".$telefono_evento."', '".$nome_organizzatore_evento."', '".$descrizione_evento."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato un nuovo EVENTO<br><br>
             ".$nome_evento."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO EVENTO Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
