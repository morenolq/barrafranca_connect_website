<?php

if (!isset($_COOKIE["cookie_connection_barrafrancaonline"])){
    header("location: index.php");
}

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  nuovi_negozi_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

                echo "#NOMENEGOZIO","<br>";
                echo $row["nome_negozio"], "<br>";
                echo "#INDIRIZZONEGOZIO", "<br>";
                echo $row["indirizzo_negozio"], "<br>";
                echo "#DESCRIZIONENEGOZIO", "<br>";
                echo $row["descrizione_negozio"], "<br>";

                if(!empty($row["telefono_negozio"])){
                        echo "#TELEFONONEGOZIO", "<br>";
                        echo $row["telefono_negozio"], "<br>";
                }

                if(!empty($row["email_negozio"])){
                        echo "#EMAILNEGOZIO", "<br>";
                        echo $row["email_negozio"], "<br>";
                }

                echo "#CATEGORIANEGOZIO", "<br>";
                echo $row["categoria_negozio"], "<br>";
                echo "#COMECONTATTARENEGOZIO", "<br>";
                echo $row["come_contattarti"], "<br>";
                echo "#FINENEGOZIO";
                echo "<br>", "<br>";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
