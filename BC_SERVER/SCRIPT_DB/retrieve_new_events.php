<?php

if (!isset($_COOKIE["cookie_connection_barrafrancaonline"])){
    header("location: index.php");
}

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  nuovi_eventi_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

                echo "#NOMEEVENTO","<br>";
                echo $row["nome_evento"], "<br>";
                echo "#DATAEVENTO", "<br>";
                echo $row["data_evento"], "<br>";
                echo "#ORARIOEVENTO", "<br>";
                echo $row["ora_evento"], "<br>";

                if(!empty($row["email_evento"])){
                        echo "#EMAILCONTATTOEVENTO", "<br>";
                        echo $row["email_evento"], "<br>";
                }


                if(!empty($row["telefono_evento"])){
                        echo "#TELEFONOCONTATTOEVENTO", "<br>";
                        echo $row["telefono_evento"], "<br>";
                }

                echo "#ORGANIZZATOREEVENTO", "<br>";
                echo $row["nome_organizzatore_evento"], "<br>";
                echo "#DESCRIZIONEEVENTO", "<br>";
                echo $row["descrizione_evento"], "<br>";
                echo "#COMECONTATTAREEVENTO", "<br>";
                echo $row["come_contattarti"], "<br>";
                echo "#FINEEVENTO", "<br>";
                echo "<br>", "<br>";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
