<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM eventi_barrafranca
        WHERE datediff(CURDATE(), STR_TO_DATE(data_evento, '%d/%m/%Y')) <= 2
        ORDER BY STR_TO_DATE(data_evento, '%d/%m/%Y') ASC";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

                echo "#NOMEEVENTO","\n";
                echo $row["nome_evento"], "\n";
                echo "#DATAEVENTO", "\n";
                echo ($row["data_evento"]), "\n";
                echo "#ORARIOEVENTO", "\n";
                echo ($row["ora_evento"]), "\n";

                if(!empty(($row["email_evento"]))){
                        echo "#EMAILCONTATTOEVENTO", "\n";
                        echo ($row["email_evento"]), "\n";
                }


                if(!empty(($row["telefono_evento"]))){
                        echo "#TELEFONOCONTATTOEVENTO", "\n";
                        echo ($row["telefono_evento"]), "\n";
                }

                echo "#ORGANIZZATOREEVENTO", "\n";
                echo ($row["nome_organizzatore_evento"]), "\n";
                echo "#DESCRIZIONEEVENTO", "\n";
                echo ($row["descrizione_evento"]), "\n";
                echo "#COMECONTATTAREEVENTO", "\n";
                echo ($row["come_contattarti"]), "\n";
                echo "#FINEEVENTO", "\n";
                echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
