<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_consiglio = mysqli_real_escape_string($conn, $_GET["nome_consiglio"]);
$descrizione_consiglio = mysqli_real_escape_string($conn, $_GET["descrizione_consiglio"]);

$sql = "INSERT INTO avvisi_barrafranca (nome_consiglio, descrizione_consiglio)
VALUES ('".$nome_consiglio."', '".$descrizione_consiglio."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato un CONSIGLIO<br><br>
             ".$nome_consiglio."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO CONSIGLIO Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
