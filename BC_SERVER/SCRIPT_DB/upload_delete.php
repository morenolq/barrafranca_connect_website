<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_consiglio = mysqli_real_escape_string($conn, $_GET["nome_cancellazione"]);
$descrizione_consiglio = mysqli_real_escape_string($conn, $_GET["motivo_cancellazione"]);
$categoria_cancellazione = mysqli_real_escape_string($conn, $_GET["categoria_cancellazione"]);
$come_contattarti = mysqli_real_escape_string($conn, $_GET["come_contattarti"]);

$sql = "INSERT INTO cancellazione_barrafranca (nome_cancellazione, motivo_cancellazione, categoria_cancellazione, come_contattarti)
VALUES ('".$nome_consiglio."', '".$descrizione_consiglio."', '".$categoria_cancellazione."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato una nuova CANCELLAZIONE<br><br>
             ".$nome_consiglio."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVA CANCELLAZIONE Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
