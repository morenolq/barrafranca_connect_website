<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_negozio = mysqli_real_escape_string($conn, $_POST["nome_negozio"]);
$email_negozio = mysqli_real_escape_string($conn, $_POST["email_negozio"]);
$telefono_negozio = mysqli_real_escape_string($conn, $_POST["telefono_negozio"]);
$indirizzo_negozio = mysqli_real_escape_string($conn, $_POST["indirizzo_negozio"]);
$categoria_negozio = mysqli_real_escape_string($conn, $_POST["categoria_negozio"]);
$descrizione_negozio = mysqli_real_escape_string($conn, $_POST["descrizione_negozio"]);
$come_contattarti = mysqli_real_escape_string($conn, $_POST["come_contattarti"]);


$sql = "INSERT INTO nuovi_negozi_barrafranca (nome_negozio, indirizzo_negozio, email_negozio, telefono_negozio, categoria_negozio, descrizione_negozio, come_contattarti)
VALUES ('".$nome_negozio."', '".$indirizzo_negozio."', '".$email_negozio."', '".$telefono_negozio."', '".$categoria_negozio."', '".$descrizione_negozio."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato un nuovo NEGOZIO<br><br>
             ".$nome_negozio."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO NEGOZIO Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
