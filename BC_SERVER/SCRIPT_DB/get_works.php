<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  lavori_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

              echo "#MANSIONELAVORO","\n";
              echo $row["nome_lavoro"], "\n";
              echo "#NOMECONTATTO", "\n";
              echo $row["nome_contatto_lavoro"], "\n";

              if(!empty($row["email_lavoro"])){
                      echo "#EMAILCONTATTO", "\n";
                      echo $row["email_lavoro"], "\n";
              }

              if(!empty($row["telefono_lavoro"])){
                      echo "#TELEFONOCONTATTO", "\n";
                      echo $row["telefono_lavoro"], "\n";
              }

              echo "#INDIRIZZOLAVORO", "\n";
              echo $row["indirizzo_lavoro"], "\n";
              echo "#DESCRIZIONELAVORO", "\n";
              echo $row["descrizione_lavoro"], "\n";
              echo "#COMECONTATTARELAVORO", "\n";
              echo $row["come_contattarti"], "\n";
              echo "#FINELAVORO";
              echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
