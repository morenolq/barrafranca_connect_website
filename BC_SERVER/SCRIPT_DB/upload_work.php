<?php

include("connect_db.php");
include("utility_php_bc.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$nome_lavoro = mysqli_real_escape_string($conn, $_GET["nome_lavoro"]);
$nome_contatto_lavoro = mysqli_real_escape_string($conn, $_GET["nome_contatto_lavoro"]);
$email_lavoro = mysqli_real_escape_string($conn, $_GET["email_lavoro"]);
$telefono_lavoro = mysqli_real_escape_string($conn, $_GET["telefono_lavoro"]);
$indirizzo_lavoro = mysqli_real_escape_string($conn, $_GET["indirizzo_lavoro"]);
$descrizione_lavoro = mysqli_real_escape_string($conn, $_GET["descrizione_lavoro"]);
$come_contattarti = mysqli_real_escape_string($conn, $_GET["come_contattarti"]);


$sql = "INSERT INTO nuovi_lavori_barrafranca (nome_lavoro, nome_contatto_lavoro, email_lavoro, telefono_lavoro, indirizzo_lavoro, descrizione_lavoro, come_contattarti)
VALUES ('".$nome_lavoro."', '".$nome_contatto_lavoro."', '".$email_lavoro."', '".$telefono_lavoro."', '".$indirizzo_lavoro."', '".$descrizione_lavoro."', '".$come_contattarti."')";

if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    $body = "Salve, è stato caricato un nuovo LAVORO<br><br>
             ".$nome_lavoro."<br>
             <br>
             Il Team di Barrafranca Connect";
    sendEmail("amministrazione@barrafrancaconnect.com", "NUOVO LAVORO Barrafranca Connect", $body);
    return_home("Il caricamento è avvenuto correttamente! Verrà valutato e inserito il prima possibile, grazie.");
    return;
} else {
    mysqli_close($conn);
    return_home("Errore durante il caricamento, ci scusiamo per l'inconveniente, riprova più tardi.");
}

mysqli_close($conn);


?>
