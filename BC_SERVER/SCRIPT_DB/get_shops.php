<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  negozi_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

              echo "#NOMENEGOZIO","\n";
              echo $row["nome_negozio"], "\n";
              echo "#INDIRIZZONEGOZIO", "\n";
              echo $row["indirizzo_negozio"], "\n";
              echo "#DESCRIZIONENEGOZIO", "\n";
              echo $row["descrizione_negozio"], "\n";

              if(!empty($row["telefono_negozio"])){
                      echo "#TELEFONONEGOZIO", "\n";
                      echo $row["telefono_negozio"], "\n";
              }

              if(!empty($row["email_negozio"])){
                      echo "#EMAILNEGOZIO", "\n";
                      echo $row["email_negozio"], "\n";
              }

              echo "#CATEGORIANEGOZIO", "\n";
              echo $row["categoria_negozio"], "\n";
              echo "#COMECONTATTARENEGOZIO", "\n";
              echo $row["come_contattarti"], "\n";
              echo "#FINENEGOZIO";
              echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
