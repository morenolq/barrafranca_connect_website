<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  offerte_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

              echo "#NOMEOFFERTA","\n";
              echo $row["nome_offerta"], "\n";
              echo "#NOMENEGOZIOOFFERTA","\n";
              echo $row["nome_negozio"], "\n";
              echo "#VALIDITAOFFERTA", "\n";
              echo $row["data_inizio_offerta"] . " - " . $row["data_fine_offerta"], "\n";

              if(!empty($row["link_pdf"])){
                      echo "#PDFURLOFFERTA", "\n";
                      echo "www.barrafrancaconnect.com/".$row["link_pdf"], "\n";
              }

              if(!empty($row["links_img"])){
                      echo "#STARTIMAGESURLOFFERTA", "\n";
                      echo $row["links_img"], "\n";
                      echo "#ENDIMAGESURLOFFERTA", "\n";
              }

              echo "#FINEOFFERTA";
              echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
