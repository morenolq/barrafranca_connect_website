<?php

header("Content-Type: text/html;charset=utf-8");

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  comprovendo_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "\n" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

              echo "#TITOLOANNUNCIO","\n";
              echo $row["nome_comprovendo"], "\n";
              echo "#PREZZOANNUNCIO", "\n";
              echo $row["prezzo_comprovendo"], "\n";
              echo "#NOMECONTATTOANNUNCIO", "\n";
              echo $row["nome_contatto_comprovendo"], "\n";

              if(!empty($row["email_comprovendo"])){
                      echo "#EMAILCONTATTOANNUNCIO", "\n";
                      echo $row["email_comprovendo"], "\n";
              }


              if(!empty($row["telefono_comprovendo"])){
                      echo "#TELEFONOCONTATTOANNUNCIO", "\n";
                      echo $row["telefono_comprovendo"], "\n";
              }

              echo "#DESCRIZIONEANNUNCIO", "\n";
              echo $row["descrizione_comprovendo"], "\n";
              echo "#COMECONTATTAREANNUNCIO", "\n";
              echo $row["come_contattarti"], "\n";
              echo "#FINEANNUNCIO", "\n";
              echo "\n", "\n";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
