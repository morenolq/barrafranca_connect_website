<?php

if (!isset($_COOKIE["cookie_connection_barrafrancaonline"])){
    header("location: index.php");
}

include("connect_db.php");
// Create connection
$conn = getConnection();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    echo "ERRORE connessione database";
}

mysqli_query($conn, "SET NAMES 'utf8'");
mysql_set_charset("utf8");

$sql = "SELECT * FROM  nuove_offerte_barrafranca";

$result = mysqli_query($conn, $sql);

if (!$result) {
    echo "ERRORE SELECT: " . $sql . "<br>" . mysqli_error($conn);
} else {
    if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {

                echo "#NEGOZIOOFFERTA","<br>";
                echo $row["nome_offerta"], "<br>";
                echo "#VALIDITAOFFERTA", "<br>";
                echo $row["data_inizio_offerta"] . " - " . $row["data_fine_offerta"], "<br>";

                if(!empty($row["link_pdf"])){
                        echo "#PDFURLOFFERTA", "<br>";
                        echo $row["link_pdf"], "<br>";
                }

                if(!empty($row["links_img"])){
                        echo "#STARTIMAGESURLOFFERTA", "<br>";
                        echo $row["links_img"], "<br>";
                        echo "#ENDIMAGESURLOFFERTA", "<br>";
                }

                echo "#FINEOFFERTA";
                echo "<br>", "<br>";

            }
        } else {
            echo "0 results";
        }

}

mysqli_close($conn);


 ?>
