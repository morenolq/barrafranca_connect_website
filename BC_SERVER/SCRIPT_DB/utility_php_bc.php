<?php

function mysqlCleaner($conn, $data)
{
	$data = mysqli_real_escape_string($conn, $data);
	$data = stripslashes($data);
	$data = htmlentities($data);
	return $data;
	//or in one line code
	//return(stripslashes(mysql_real_escape_string($data)));
}

function encryptPassword($password, $sale){

	$saltedPW =  $password . $sale;
	$hashedPW = hash('sha256', $saltedPW);
	return $hashedPW;

}

function destroysession(){
	$_SESSION = array();
	if (ini_get("session.use_cookies")){
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time()- 3600*24, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	}
	session_destroy();
}

function return_home($message){

	setcookie("messageHomeBarrafrancaConnect", $message, time() + 5, "/");
	header("location: Barrafranca_Connect/index.php");

}

function returnErrorLogin($message){

	setcookie("messageLoginBarrafrancaConnect", $message, time() + 5, "/");
	header("location: ../login.php");

}

function sendEmail($to, $subject, $body){
	//require "../PHPmailer/PHPMailerAutoload.php";
	require_once('../../PHPMailer/class.phpmailer.php');
  require_once('../../PHPMailer/class.smtp.php');

  $mail = new PHPMailer(); // create a new object
  $mail->IsSMTP(); // enable SMTP
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
  $mail->SMTPDebug = 0;
  $mail->SMTPAuth = true; // authentication enabled
  $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
  $mail->Host = "smtp.1and1.com";
  $mail->Port = 25; // 25 or 587 --- 465
  $mail->IsHTML(true);
  $mail->Username = "amministrazione@barrafrancaconnect.com";
  $mail->Password = "LogicSwift1993";
  $mail->SetFrom("amministrazione@barrafrancaconnect.com");
  $mail->addReplyTo( 'amministrazione@barrafrancaconnect.com', 'Amministrazione Barrafranca Connect' );
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AddAddress($to);

	return $mail->send();

}

?>
