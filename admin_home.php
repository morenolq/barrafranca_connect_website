<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="ADMINISTRATOR"){
    returnHome("Errore, non hai il diritto di entrare nel pannello di amministrazione.");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    function getEventi(){
      location.href = "./admin_action.php?action=eventi";
    }

    function getNegozi(){
      location.href = "./admin_action.php?action=negozi";
    }

    function getLavori(){
      location.href = "./admin_action.php?action=lavori";
    }

    function getOfferte(){
      location.href = "./admin_action.php?action=offerte";
    }

    function getComproVendo(){
      location.href = "./admin_action.php?action=comprovendo";
    }

    function getConsigli(){
      location.href = "./admin_action.php?action=consigli";
    }

    function getCancellazioni(){
      location.href = "./admin_action.php?action=cancellazioni";
    }

    function getFarmacie(){
      location.href = "./admin_action.php?action=farmacie";
    }

  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php //MESSAGE + WELCOME
            if (isset($_COOKIE['messageAdminHomeBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageAdminHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageAdminHomeBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageAdminHomeBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageAdminHomeBarrafrancaConnect']);
              }
            }

            if (isset($_SESSION['username'])){
                echo "<div class='row'>";
                  echo "<div class='col s12 m12'>";
                    echo "<div class='card-panel blue darken-2'>";
                      echo "<span class='white-text'> Benvenuto " . $_SESSION['username'] . "<br>";
                      echo "Da grande potere derivano <b>grandi responsabilità</b>, quindi, non fare <b>cazzate!</b>. ";
                      echo "Se ha i dubbi o problemi, prima di fare qualcosa, chiedi consiglio a Moreno La Quatra. (Se tu sei Moreno La Quatra, mbare, un fari minchiati!)";
                      echo "</span>";
                    echo "</div>";
                  echo "</div>";
                echo "</div>";
              }
          ?>
        </div>

        <div class="row">
          <div class="input-field col s12 m6">
            <button onclick="location.href='change_password.php'" style="width:100%" class="btn waves-effect orange darken-2" align="center">Cambia Password</button>
          </div>
          <div class="input-field col s12 m6">
            <button onclick="location.href='PHP_script/logout_script.php'" style="width:100%" class="btn waves-effect orange darken-2" align="center">Esci</button>
          </div>
        </div>


        <ul class="collection with-header">
          <li class="collection-header"><h4>Ecco le azioni che puoi fare</h4></li>
          <li class="collection-item"><div>Eventi<a href="./admin_action.php?action=eventi" onclick="getEventi()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Negozi<a href="./admin_action.php?action=negozi" onclick="getNegozi()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Lavori<a href="./admin_action.php?action=lavori" onclick="getLavori()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Offerte<a href="./admin_action.php?action=offerte" onclick="getOfferte()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Compro/Vendo<a href="./admin_action.php?action=comprovendo" onclick="getComproVendo()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Consigli<a href="./admin_action.php?action=consigli" onclick="getConsigli()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Cancellazioni<a href="./admin_action.php?action=cancellazioni" onclick="getCancellazioni()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
          <li class="collection-item"><div>Farmacie<a href="./admin_action.php?action=farmacie" onclick="getFarmacie()" class="secondary-content"><i class="material-icons">send</i></a></div></li>
        </ul>



    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'"  class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
