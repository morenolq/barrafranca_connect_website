<?php

//Detect special conditions devices
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

//do something with this information
if( $iPod || $iPhone || $iPad){
  //browser reported as an iPhone/iPod touch -- do something here
  header('Location: https://itunes.apple.com/app/id1274021936');
}else if($Android){
  //browser reported as an Android device -- do something here
  header('Location: http://play.google.com/store/apps/details?id=com.mlqlab.barrafrancaconnect');
}else {
  header('Location: http://play.google.com/store/apps/details?id=com.mlqlab.barrafrancaconnect');
}

?>
