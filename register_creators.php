<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if (isset($_SESSION["role"])){
    returnHome("Esci dal tuo account prima di registrarti.");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="js/jquery-3.2.1.min.js"></script>
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    function validateCreatorsForm(){
      nome_creator = document.getElementById("nome_creator").value;
      email_account_creator = document.getElementById("email_account_creator").value;
      password_account_creator1 = document.getElementById("password_account_creator1").value;
      password_account_creator2 = document.getElementById("password_account_creator2").value;
      cbInfo = document.getElementById("cbInfo").checked;


      if (nome_creator.trim() === "" || email_account_creator.trim() === "" ||
          password_account_creator1.trim() === "" || password_account_creator2.trim() === ""){
            alert("Attenzione, devono essere riempiti tutti i campi.");
            return;
          }

      if (!isValidEmail(email_account_creator)){
        alert("L'email del negozio inserita non è valida.");
        return;
      }

      if(password_account_creator1 !== password_account_creator2){
        alert("Le password non coincidono.");
        return;
      }

      if (!cbInfo){
        alert("Devi accettare la normativa sulla privacy");
        return;
      }

      document.getElementById("creators-signin-form").submit();

    }
  </script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Registrati come creatore</h3>
      <center><p>oppure se sei già registrato <a href="login.php">clicca qui</a></p></center>
      <center>
        <form class="col s12" method="post" action="PHP_script/insert_creator.php" id="creators-signin-form" enctype=”multipart/form-data”>

          <div class="input-field col s12">
            <input id="nome_creator" name="nome_creator" type="text" class="validate">
            <label for="nome_creator">Nome e Cognome</label>
          </div>

          <div class="input-field col s12">
            <input id="email_account_creator" name="email_account_creator" type="email" class="validate">
            <label for="email_account_creator" data-error="Inserisci un email valida">Email account</label>
          </div>

          <div class="input-field col s12">
            <input id="password_account_creator1" name="password_account_creator1" type="password" class="validate">
            <label for="password_account_creator1">Password Account</label>
          </div>

          <div class="input-field col s12">
            <input id="password_account_creator2" name="password_account_creator2" type="password" class="validate">
            <label for="password_account_creator2">Ripeti Password Account</label>
          </div>

          <p class="input-field col s12">
            <input type="checkbox" id="cbInfo"/>
            <label for="cbInfo">Accetto l'<a href="./info_privacy.php">informativa sulla privacy</a></label>
          </p>

          <br>

          <button type="button" class="btn waves-effect blue" onclick="validateCreatorsForm()">Invio
            <i class="material-icons right">send</i>
          </button>
        </form>
      </center>

    </div>
  </div>

  <br><br>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
          <!--<p class="grey-text text-lighten-4">Usando il sito si accetta l'<a style="color:red;" href="./info_privacy.php">informativa sulla privacy</p>-->
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
