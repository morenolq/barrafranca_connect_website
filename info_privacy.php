<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>

</head>
<body>

  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Informativa sulla privacy</h3>

<br>

        <h5>Informativa Privacy ai sensi dell’art.13 D.lgs. 196/2003</h5>

<br>

<p>Barrafranca Connect (City Connect) è consapevole dell’importanza della tutela dei dati personali e, considerato che internet è potenzialmente un grande strumento di scambio e circolazione di informazioni, si impegna a rispettare le regole di condotta sancite dal decreto legislativo 30 giugno 2003 n° 196 (codice in materia di protezione dei dati personali) dirette ad assicurare una controllata e garantita navigazione in rete nonché la tutela delle persone e degli altri soggetti indicati da detta normativa, con riferimento al trattamento dei dati personali.
Tale trattamento sarà improntato, in armonia con la normativa indicata, ai principi di correttezza, liceità e di tutela dei diritti dei soggetti ivi previsti. Barrafranca Connect (City Connect) desidera, inoltre, informarLa che i Suoi dati personali saranno trattati nel rispetto delle normative vigenti, nelle forme e nei limiti prescritti dalla legge.
Ai sensi dell’articolo 13 del D.lgs. n.196/2003, ove applicabile, Le forniamo le seguenti informazioni:</p>

<p>1. I dati da Lei forniti verranno trattati per la Sua registrazione al sito, per consentirLe l’accesso all’area riservata del dominio barrafrancaconnect.com nonché la fruizione del servizio di informazione che il sito si propone in relazione alle tematiche ed alle finalità che Barrafranca Connect (City Connect) tratta. Tale servizio informativo potrà avvenire anche tramite newsletter o invio di e-mail allo scopo di promuovere le iniziative poste in essere dalla nostra azienda, ovvero allo scopo di promuovere convegni o studi, approfondimenti sulle tematiche di nostro interesse quand’anche provenienti da soggetti terzi.
Le informazioni da noi inviate potranno rivestire anche carattere commerciale.
Il trattamento dei suoi dati potrà, inoltre, essere finalizzato all’adempimento di obblighi civilistici, fiscali, contabili nonché di gestione amministrativa conseguenti ad eventuali rapporti giuridici che potranno essere instaurati in occasione della Sua navigazione sul dominio barrafrancaconnect.com.</p>

<p>2. Il trattamento sarà effettuato sia manualmente sia con l’ausilio di supporti informatici sempre nel rispetto delle prescrizioni di cui all’art.11 D.lgs. n.196/2003. I dati personali raccolti con la presente registrazione potranno essere trattati da incaricati del trattamento preposti alla gestione dei servizi richiesti ed alle attività di marketing.</p>

<p>3. Il conferimento dei dati è:</p>

<p>– obbligatorio relativamente ai dati identificati come “campi obbligatori” e l’eventuale rifiuto di fornire tali dati comporterà la mancata esecuzione della registrazione; ciò in quanto i dati in oggetto assumono valenza identificativa per consentirLe l’accesso all’area riservata del sito;</p>
<p>– facoltativo relativamente ai dati identificati come “campi facoltativi”; l’eventuale omessa fornitura di tali dati non pregiudicherà l’erogazione del servizio.</p>

<p>4. I dati potranno essere comunicati:</p>
<p>– a società controllate e collegate, soggetti privati, associazioni, fondazioni enti od organismi senza scopi di lucro, persone giuridiche, società di persone o di capitali, imprese individuali esclusivamente per i fini connessi all’erogazione del servizio cui Lei ha aderito;</p>
<p>– a soggetti privati e persone giuridiche che svolgono attività strumentali connesse o di supporto a quelle svolte da Barrafranca Connect (City Connect) per l’esecuzione delle operazioni o dei servizi cui Lei ha aderito, le quali agiranno quali autonomi titolari del trattamento dei dati conferiti;</p>
<p>– a enti e organismi pubblici che hanno per legge, regolamento, direttiva comunitaria obbligo o diritto a conoscerli.</p>


<p>
5. Il titolare del trattamento è Barrafranca Connect (City Connect).
</p>

<p>6. I dati oggetto di trattamento potranno essere trasferiti anche in Paesi dell’UE od in paesi terzi, ed il Suo consenso si intende fornito anche ex. Art. 43 c.1 lettera a) dlgs 196/03.</p>

<p>8. In ogni momento i soggetti destinatari delle tutele di cui al D.lgs.196/2003 potranno esercitare i diritti previsti dall’art. 7 e s.m.i. che per comodità sono riprodotti di seguito:
“L’interessato ha diritto di ottenere la conferma dell’esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile.
</p>

<p>L’interessato ha diritto di ottenere l’indicazione:</p>
<p>a) dell’origine dei dati personali;</p>
<p>b) delle finalità e modalità del trattamento;</p>
<p>c) della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti elettronici;</p>
<p>d) degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell’articolo 5, comma 2;</p>
<p>e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati.</p>


<p>L’interessato ha diritto di ottenere:</p>
<p>a) l’aggiornamento, la rettificazione ovvero, quando vi ha interesse, l’integrazione dei dati;</p>
<p>b) la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;</p>
<p>c) l’attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato.</p>
<p>L’interessato ha diritto di opporsi, in tutto o in parte:</p>
<p>a) per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti allo scopo della raccolta;</p>
<p>b) al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale”.</p>


    </div>
  </div>


  <div class="container">
    <div class="section">



    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'"  class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

</body>
</html>
