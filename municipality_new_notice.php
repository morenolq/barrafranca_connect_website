<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="MUNICIPALITY"){
    returnHome("Errore, pannello riservato all'amministrazione comunale.");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/materialize.clockpicker.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="/js/pdfobject.js"></script>
  <script src="./js/pdfobject.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/materialize.clockpicker.js"></script>
  <script src="js/init.js"></script>

  <style>
	.pdfobject-container { height: 500px;}
	.pdfobject { border: 1px solid #666; }
  </style>

</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php //MESSAGE + WELCOME
            if (isset($_COOKIE['messageMunicipalityHomeBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageMunicipalityHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageMunicipalityHomeBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageMunicipalityHomeBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageMunicipalityHomeBarrafrancaConnect']);
              }
            }

            if (isset($_SESSION['username'])){
                printCardBlue("Benvenuto <b>" . $_SESSION['username'] . "</b>");
              }

          ?>

          <center>
            <form class="col s12" method="post" action="PHP_script/upload_notice_municipality.php" id="form-notice-municipality" enctype="multipart/form-data">

              <div class="row">
                <div class="input-field col s12">
                  <input id="nome_avviso" name="nome_avviso" type="text" class="validate">
                  <label for="nome_avviso">Nome Avviso</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Data Avviso" id="data_avviso" name="data_avviso" type="date" class="datepicker">
                </div>
              </div>

              <div class="input-field col s12">
  		          <i class="material-icons prefix">mode_edit</i>
                  <textarea id="descrizione_avviso" name="descrizione_avviso" class="materialize-textarea"></textarea>
                  <label for="descrizione_avviso">Descrizione</label>
              </div>

              <div class="row">
                <div class='file-field input-field'>
                  <div class='btn waves-effect red'>
                    <span>Allegato Avviso (PDF)</span>
                    <input id='PDFFile' name='PDFFile' type='file' single>
                  </div>
                  <div class='file-path-wrapper'>
                    <input id='fileName' name='fileName' onchange="verifyExtension()" class='file-path validate' type='text' accept='application/pdf' placeholder='Seleziona PDF avviso. (NON obbligatorio)'>
                  </div>
                </div>
              </div>



              <div class="row">
                <div class="input-field col s12">
                  <input id="link_avviso" name="link_avviso" type="text" class="validate">
                  <label for="link_avviso">Link Avviso (Non Obblgatorio)</label>
                </div>
              </div>

              <div class="row" align="center">
                <div class="input-field col s12">
                  <input type="checkbox" id="notify_avviso" name="notify_avviso"/>
                  <label for="notify_avviso">Inviare Notifica</label>
                </div>
              </div>

              <br>

              <button type="button" class="btn waves-effect blue" onclick="validateNoticeForm()">Invio
                <i class="material-icons right">send</i>
              </button>
            </form>
          </center>

        </div>

    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>

  <script>
    $( document ).ready(function() {
      $('.datepicker').pickadate({
        monthsFull: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
        monthsShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
        weekdaysFull: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
        weekdaysLetter: [ 'D', 'L', 'M', 'M', 'G', 'V', 'S' ],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
        labelMonthNext: 'Mese prossimo',
        labelMonthPrev: 'Mese precedente',
        labelMonthSelect: 'Seleziona Mese',
        labelYearSelect: 'Seleziona anno',
        today: 'Oggi',
        clear: 'Cancella',
        close: 'Chiudi',
        firstDay: 'Lunedì',
        closeOnSelect: true,
        format: 'dd/mm/yyyy',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
      });

      $('.timepicker').clockpicker({
        default: 'now', // Set default time
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Cancella', // text for clear-button
        canceltext: 'Annulla', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true,// make AM PM clickable
        formatSubmit: 'HH:i',
        //aftershow: function(){} //Function for after opening timepicker
      });

    });

    function validateNoticeForm(){
      nome_avviso = document.getElementById("nome_avviso").value;
      data_avviso = document.getElementById("data_avviso").value;
      descrizione_avviso = document.getElementById("descrizione_avviso").value;
      link_avviso = document.getElementById("link_avviso").value;

      if (nome_avviso.trim() === ""){
        alert("Inserire un nome per l'avviso.");
        return;
      }

      if (data_avviso.trim() === ""){
        alert("Inserire una data per l'avviso.");
        return;
      }

      if (descrizione_avviso.trim() === ""){
        alert("Inserire una descrizione per l'avviso.");
        return;
      }

      filename = document.getElementById("fileName").value;
      extension = filename.split('.').pop();

      if (filename!== "" && extension !== "pdf"){
        alert("Il file caricato non è un pdf, per favore carica un file valido.");
        document.getElementById("fileName").value = "";
        return;
      }

      document.getElementById("form-notice-municipality").submit();

    }

    function verifyExtension() {
      filename = document.getElementById("fileName").value;
      extension = filename.split('.').pop();
      if (extension !== "pdf"){
        alert("Il file caricato non è un pdf, per favore carica un file valido.");
        document.getElementById("fileName").value = "";
      }
    }


  </script>

  </body>
</html>
