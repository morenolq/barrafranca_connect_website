<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Barrafranca Connect</a>

      <?php
        if (isset($_SESSION['username']) && isset($_SESSION['role'])){

          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";

          if ($_SESSION['role'] === "ADMINISTRATOR"){
            echo "<ul class='right hide-on-med-and-down'>";
              echo "<li><a href='./admin_home.php'>Home Admin</a></li>";
            echo "</ul>";
          } else if ($_SESSION['role'] === "USER_RED"){
            echo "<ul class='right hide-on-med-and-down'>";
              echo "<li><a href='./userred_home.php'>Pagina Personale</a></li>";
            echo "</ul>";
          } else if ($_SESSION['role'] === "CREATOR"){
            echo "<ul class='right hide-on-med-and-down'>";
              echo "<li><a href='./creator_home.php'>Pagina Personale</a></li>";
            echo "</ul>";
          }

        } else {
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='login.php'>Entra nell'area personale</a></li>";
          echo "</ul>";
        }

      ?>


      <!--<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>-->
    </nav>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container" align="center">
      <br><br>
      <h2 class="header center orange-text">Barrafranca Connect</h2>
      <div class="row center">
        <h5 class="header col s12 light">Semplicemente, per tutti.</h5>
      </div>
      <?php //MESSAGE PRINT
        if (isset($_COOKIE['messageHomeBarrafrancaConnect'])){
          if (strpos($_COOKIE['messageHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageHomeBarrafrancaConnect'], 'error') !== false ) {
            echo "<div class='row'>";
              echo "<div class='col s12 m12'>";
                echo "<div class='card-panel red'>";
                  echo "<span class='white-text flow-text'>" . $_COOKIE['messageHomeBarrafrancaConnect'];
                  echo "</span>";
                echo "</div>";
              echo "</div>";
            echo "</div>";
          } else {
            echo "<div class='row'>";
              echo "<div class='col s12 m12'>";
                echo "<div class='card-panel green'>";
                  echo "<span class='white-text flow-text'>" . $_COOKIE['messageHomeBarrafrancaConnect'];
                  echo "</span>";
                echo "</div>";
              echo "</div>";
            echo "</div>";
          }
        }
      ?>
      <div class="row center">
        <img onclick="location.href='download_app.php'" src="./img/google-play-badge.png" alt="Google Play Download" style="width:190px;height:70px;">
        <img onclick="location.href='download_app.php'" src="./img/app-store-icon.svg" alt="Apple Store Download" style="width:160px;height:70px;">
      </div>
      <br>

      <div class="row" align="center">
      <?php

        if (isset($_SESSION['username']) && isset($_SESSION['role'])){

          if ($_SESSION['role'] === "USER_RED"){
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='userred_home.php'\" id='userred-home-button' class='btn waves-effect waves-light orange'>Pagina Personale</button>";
            echo "</div>";
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='./PHP_script/logout_script.php'\" id='logout-button' class='btn waves-effect waves-light orange'>Logout</button>";
            echo "</div>";
          } else if ($_SESSION['role'] === "ADMINISTRATOR"){
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='admin_home.php'\" id='admin-home-button' class='btn waves-effect waves-light orange'>Admin Home</button>";
            echo "</div>";
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='./PHP_script/logout_script.php'\" id='logout-button' class='btn waves-effect waves-light orange'>Logout</button>";
            echo "</div>";
          } else if ($_SESSION['role'] === "CREATOR"){
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='creator_home.php'\" id='admin-home-button' class='btn waves-effect waves-light orange'>Pagina Personale</button>";
            echo "</div>";
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='./PHP_script/logout_script.php'\" id='logout-button' class='btn waves-effect waves-light orange'>Logout</button>";
            echo "</div>";
          } else if ($_SESSION['role'] === "MUNICIPALITY"){
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='municipality_home.php'\" id='admin-home-button' class='btn waves-effect waves-light orange'>Pagina Personale</button>";
            echo "</div>";
            echo "<div class='input-field col s12 m6' align='center'>";
              echo "<button align='center' style='width:100%' onclick=\"location.href='./PHP_script/logout_script.php'\" id='logout-button' class='btn waves-effect waves-light orange'>Logout</button>";
            echo "</div>";
          }

        } else {
          echo "<div class='input-field col s12 m4' align='center'>";
            echo "<button align='center' style='width:100%' onclick=\"location.href='login.php'\" id='login-button' class='btn waves-effect waves-light orange'>Login</button>";
          echo "</div>";
          echo "<div class='input-field col s12 m4' align='center'>";
            echo "<button align='center' style='width:100%' onclick=\"location.href='signin_red.php'\" id='plan-button' class='btn waves-effect waves-light orange'>Iscriviti</button>";
          echo "</div>";
          echo "<div class='input-field col s12 m4' align='center'>";
            echo "<button align='center' style='width:100%' onclick=\"location.href='register_creators.php'\" id='creators-button' class='btn waves-effect waves-light orange'>Creatori(Foto)</button>";
          echo "</div>";
          //echo "<div class='input-field col s12 m4' align='center'>";
            //echo "<button align='center' style='width:100%' onclick=\"location.href='migrate.php'\" id='upgrade-button' class='btn waves-effect waves-light orange'>Upgrade a Rosso</button>";
          //echo "</div>";
        }

      ?>
      </div>

    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center blue-text"><i class="material-icons">verified_user</i></h2>
            <h5 class="center">Concreta</h5>

            <p class="light" align="center">Informazioni utili e tangibili che possono migliorare la vita quotidiana dei cittadini.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">Collaborativa</h5>

            <p class="light" align="center">Con delle persone a vostra disposizione che accoglieranno i vostri consigli e risponderanno alle vostre domande.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center blue-text"><i class="material-icons">store</i></h2>
            <h5 class="center">Piena di opportunità</h5>

            <p class="light" align="center">Un'opportunità per le piccole/medie imprese, di avere il loro spazio e poter essere raggiuti con immediatezza.</p>
          </div>
        </div>
      </div>

    </div>

    <div class="section">
      <div class="row center">
        <div class="col s12">
          <button onclick="location.href='contact_us.php'" class="btn waves-effect red" align="center">Contattaci</button>
        </div>
      </div>
    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
