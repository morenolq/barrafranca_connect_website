<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="CREATOR"){
    returnHome("Errore, pannello riservato agli utenti del servizio Rosso.");
    exit;
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="/js/pdfobject.js"></script>
  <script src="./js/pdfobject.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>

  <script>
  function validateUpdateAlbumData(){
    nome_album = document.getElementById("nome_album").value;
    nome_creatore = document.getElementById("nome_creatore").value;
    descrizione_album = document.getElementById("descrizione_album").value;
    link_creatore = document.getElementById("link_creatore").value;

    if (nome_album.trim() === "" || nome_creatore.trim() === "" ||
        descrizione_album.trim() === "" || link_creatore.trim() === "" ){
          alert("Attenzione, devono essere riempiti tutti i campi.");
          return;
        }

    document.getElementById("change-album-data-form").submit();

  }

  </script>

</head>
<body>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php


          if (!isset($_SESSION["nome_creatore"])){
            returnHome("Errore, pannello riservato, fai l'accesso e riprova.");
            exit;
          }

          //for each in table print in editable. ATTENTION UPDATE SESSION VARIABLE
          $conn = getConnection();

          // Check connection
          if (!$conn) {
              goToCreatorHome("Errore di connessione con il database, riprova.");
              exit;
          }

          mysqli_query($conn, "SET NAMES 'utf8'");
          mysql_set_charset("utf8");

          $sql = "SELECT *
                  FROM  album_barrafranca
                  WHERE nome_album = '".$_GET["nome_album"]."'";

          $result = mysqli_query($conn, $sql);

          $r = mysqli_fetch_assoc($result);

          if ($r["nome_creatore"] !== $_SESSION["nome_creatore"]){
            goToCreatorHome("Errore, non hai il diritto di modificare queste informazioni.");
            exit;
          }

          echo "<center>";
            echo "<form class='col s12' method='post' action='PHP_script/change_album_data_script.php' id='change-album-data-form' enctype=”multipart/form-data”>";
              echo "<div class='row'>";
                echo "<div class='input-field col s12'>";
                  echo "<input disabled value='".$r["nome_album"]."' id='nome_album' name='nome_album' type='text' class='validate'>";
                  echo "<label for='nome_negozio'>Nome Album</label>";
                  echo "<input hidden value='".$r["nome_album"]."' id='nome_album' name='nome_album' type='text' class='validate'>";
                  echo "<label for='nome_negozio'>Nome Album</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s12'>";
                  echo "<input disabled value='".$r["nome_creatore"]."' id='nome_creatore' name='nome_creatore' type='text' class='validate'>";
                  echo "<label for='nome_creatore'>Nome Creatore</label>";
                  echo "<input hidden value='".$r["nome_creatore"]."' id='nome_creatore' name='nome_creatore' type='text' class='validate'>";
                  echo "<label for='nome_creatore'>Nome Creatore</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='input-field col s12'>";
    		      echo "<i class='material-icons prefix'>mode_edit</i>";
                echo "<textarea id='descrizione_album' name='descrizione_album' class='materialize-textarea'>".$r["descrizione_album"]."</textarea>";
                echo "<label for='descrizione_album'>Descrizione Album</label>";
              echo "</div>";
              echo "<div class='row'>";
                echo "<div class='input-field col s12'>";
                  echo "<input value='".$r["link_creatore"]."'id='link_creatore' name='link_creatore' type='text'>";
                  echo "<label for='link_creatore'>Link Creatore</label>";
                echo "</div>";
              echo "</div>";

              echo "<button type='button' class='btn waves-effect blue' onclick='validateUpdateAlbumData()'>Cambia Informazioni";
                echo "<i class='material-icons right'>done</i>";
              echo "</button>";
              echo "<p> </p>";
              echo "<button type='button' class='btn waves-effect red' onclick='location.href=\"creator_home.php\"'>Annulla";
                echo "<i class='material-icons right'>cancel</i>";
              echo "</button>";
            echo "</form>";
          echo "</center>";

        ?>

         <br>

    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
