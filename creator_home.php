<?php

  session_start();
  include("./PHP_script/utility_php_bc.php");
  if ($_SESSION["role"]!=="CREATOR"){
    returnHome("Errore, pannello riservato ai creatori (foto).");
    exit;
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Barrafranca Connect</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/loading.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
  <script type="text/javascript" src="./js/utility_bc.js"></script>
  <script src="/js/pdfobject.js"></script>
  <script src="./js/pdfobject.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>

  <script>
    function updateAbumData(albumName){
      location.href = "./update_album_data.php?nome_album=" + albumName;
    }
    function deleteAlbum(albumName){
      var r = confirm("Sei Sicuro di voler cancellare l'album?");
      if (r == true) {
        location.href = "./PHP_script/delete_album.php?nome_album=" + albumName;
      }
    }
  </script>

  <style>
	.pdfobject-container { height: 500px;}
	.pdfobject { border: 1px solid #666; }
  </style>

</head>
<body>
  <div class="loading" id="loading" style="display:none">Loading&#8230;</div>
  <nav class="blue" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Barrafranca Connect</a>
      <?php
        if (isset($_SESSION['username'])){
          echo "<ul class='right hide-on-med-and-down'>";
            echo "<li><a href='./PHP_script/logout_script.php'>Logout " . $_SESSION['username'] . "</a></li>";
          echo "</ul>";
          }
      ?>
  </nav>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br>

      <center>
        <div class="section">
          <?php //MESSAGE + WELCOME
            if (isset($_COOKIE['messageCreatorHomeBarrafrancaConnect'])){
              if (strpos($_COOKIE['messageCreatorHomeBarrafrancaConnect'], 'Error') !== false || strpos($_COOKIE['messageCreatorHomeBarrafrancaConnect'], 'error') !== false ) {
                printCardRed($_COOKIE['messageCreatorHomeBarrafrancaConnect']);
              } else {
                printCardGreen($_COOKIE['messageCreatorHomeBarrafrancaConnect']);
              }
            }

            if (isset($_SESSION['username'])){
                printCardBlue("Benvenuto <b>" . $_SESSION['nome_creatore'] . "</b>");
              }

          ?>
        </div>

        <div class="row">
          <div class="input-field col s12 m6">
            <button onclick="location.href='change_password.php'" style="width:100%" class="btn waves-effect orange darken-2" align="center">Cambia Password</button>
          </div>
          <div class="input-field col s12 m6">
            <button onclick="location.href='PHP_script/logout_script.php'" style="width:100%" class="btn waves-effect orange darken-2" align="center">Esci</button>
          </div>
        </div>

        <hr>
        <h4 class="header center orange-text">Album Caricati</h4>
        <br>
        <?php
        printAlbumsCreator($_SESSION["nome_creatore"]);
        ?>
        <br><br>
        <div class="row center">
          <div class="col s12">
            <button onclick="location.href='new_album_photo.php'" class="btn waves-effect red" align="center">Nuovo Album</button>
          </div>
        </div>

        <p class="black-text text">* Se vuoi eliminare il tuo account, <a style="color:red" href="contact_us.php">contattaci.</a></p>

    </div>
  </div>

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">City Connect</h5>
          <p class="grey-text text-lighten-4">Un idea nata per migliorare la vita quotidiana dei cittadini, usando la tecnologia alla portata di tutti. Se sei interessato a contattarci scrivici <a style="color:red" href="contact_us.php">cliccando qui</a></p>
        </div>

        <div class="col l3 s12">
          <h5 class="white-text">Scarica l'applicazione</h5>
          <ul>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="material-icons right">android</i>
              </button>
            </li>
            <br>
            <li>
              <button onclick="location.href='download_app.php'" class="btn waves-effect blue" type="submit" name="action" align="center">Scarica
                <i class="fa fa-apple" aria-hidden="true"> </i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script>
    $(document).ready(function() {
        $('select').material_select();
      });

    document.getElementById("buttonviewPDF").addEventListener("click", function() {
        PDFObject.embed(this.name, "#pdf_view");
    }, false);

  </script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>

    function verifyExtension() {
      filename = document.getElementById("fileName").value;
      extension = filename.split('.').pop();
      if (extension !== "pdf"){
        alert("Il file caricato non è un pdf, per favore carica un file valido.");
        document.getElementById("fileName").value = "";
      }
    }

    function verifyExtensionSubmit() {
      filename = document.getElementById("fileName").value;
      startDate = document.getElementById("startDate").value;
      endDate = document.getElementById("endDate").value;
      extension = filename.split('.').pop();
      if (startDate === "" || endDate === "" ){
        alert("Devi Inserire una data iniziale e finale per l'offerta");
        return;
      }
      if (extension !== "pdf"){
        alert("Il file caricato non è un pdf, per favore carica un file valido.");
        document.getElementById("fileName").value = "";
        return;
      } else {
        document.getElementById("loading").style.display = "block";
        form = document.getElementById("uploadForm").submit();
      }
    }

    $('.datepicker').pickadate({
      monthsFull: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
      monthsShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
      weekdaysFull: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
      labelMonthNext: 'Mese prossimo',
      labelMonthPrev: 'Mese precedente',
      labelMonthSelect: 'Seleziona Mese',
      labelYearSelect: 'Seleziona anno',
      today: 'Oggi',
      clear: 'Cancella',
      close: 'Chiudi',
      firstDay: 'Lunedì',
      closeOnSelect: true,
      format: 'dd/m/yyyy',
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });

  </script>

  </body>
</html>
