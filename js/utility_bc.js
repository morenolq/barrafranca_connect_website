function prenotazioni (){
  location.href = "http://localhost/PrenotOnline/prenotazioni.php";
}

function addvalueGET(id, value){

  return "&" + id + "=" + value;

}

function addfirstvalueGET(id, value){

  return id + "=" + value;

}

function return_home(){
  location.href = "https://localhost/PrenotOnline/index.php";
}

function verifyintjs(value){
  return (parseFloat(value) == parseInt(value)) && !isNaN(value);
}

function logout(){
  location.href = "https://localhost/PrenotOnline/logoutscript.php";
}

function registrati (){
  location.href = "http://localhost/PrenotOnline/registrati.php";
}

function cancellaprenotazione(){
  location.href = "https://localhost/PrenotOnline/cancella_prenotazione.php";
}

function prenota(){
  location.href = "https://localhost/PrenotOnline/inserisci_prenotazione.php";
}

function isValidEmail(email){
  var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;

  if (!emailFilter.test(email)){
    return false;
  } else {
    return true;
  }

}

function isValidPhoneNumber(tel){
  var phoneFilter = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  if (!phoneFilter.test(tel)){
    return false;
  } else {
    return true;
  }
}
